#!/bin/bash


## 96_01_check_started_services_now on 

echo "pwd =" $(pwd)
echo "USER_NAME_IN_OS = $USER_NAME_IN_OS, hostname =" $(hostname)
echo "date =" $(date)
echo "HOME = $HOME, USER = $USER, LOGNAME = $LOGNAME"
echo "By value: KAFKA_PORT = " $KAFKA_PORT ${KAFKA_PORT} "$KAFKA_PORT ${KAFKA_PORT}"
echo "By value: TOMCAT_VER = " $TOMCAT_VER ${TOMCAT_VER} "$TOMCAT_VER ${TOMCAT_VER}"

if [[ ! -z "$(cut -d= -f1 /home/$USER_NAME_IN_OS/.bashrc | grep declare)" ]]
then
	echo 'Environment variables are set.'
		echo "$(cut -d= -f1 /home/$USER_NAME_IN_OS/.bashrc | grep declare)"
fi

echo "%%%%%%%%%%%%%%%%%%"

pwd
cd $PROJECT_HOME
pwd
echo "RUNNING 96_01_check_started_services_now.sh"

echo "################### CHECK WHAT IS STARTED NOW ###################"

## sudo kill -9 PID

# ansible
sudo ps -ef | grep ansible

# docker
sudo ps -ef | grep docker

# packer
sudo ps -ef | grep packer

# terraform
sudo ps -ef | grep terraform

# 2181 Zookeeper
sudo netstat -ao | grep -i "2181"
ps -ef | grep zookeeper

# 8000 DEBUG
sudo netstat -ao | grep -i "8000"
ps -ef | grep debug

# 8080 Tomcat
sudo netstat -ao | grep -i "8080"
ps -ef | grep tomcat

# 8081 Attendant app & /hallo OR /hi
sudo netstat -ao | grep -i "8081"

# 8082 Visitor app & /hello OR /hi
sudo netstat -ao | grep -i "8082" 

# 8083 AwesomeConcepts app & /hillo OR /hi
sudo netstat -ao | grep -i "8083" 

# 8084 Eastworld app & /hollo OR /hi
sudo netstat -ao | grep -i "8084" 

# 8085 Signs app & /hullo OR /hi
sudo netstat -ao | grep -i "8085" 

# 8086 Optimizer app & /hyllo OR /hi
sudo netstat -ao | grep -i "8086" 

# 8087 SimulationScene app & /hllo OR /hi
sudo netstat -ao | grep -i "8087" 

# 8200 Vault
sudo netstat -ao | grep -i "8200" 
ps -ef | grep vault

# 8500 Consul
sudo netstat -ao | grep -i "8500"
ps -ef | grep consul

# 8042 Hadoop1_3 !!!
sudo netstat -ao | grep -i "8042"
ps -ef | grep Hadoop

# 8761 Eureka
sudo netstat -ao | grep -i "8761"
ps -ef | grep eureka

# 9000 Hadoop1_1 !!!
sudo netstat -ao | grep -i "9000"
ps -ef | grep hadoop

# 9092 Kafka
sudo netstat -ao | grep -i "9092"
ps -ef | grep kafka

# 9864 Hadoop1_4 !!!
sudo netstat -ao | grep -i "9864"
ps -ef | grep Hadoop

# 9870 Hadoop1_2 !!!
sudo netstat -ao | grep -i "9870"
ps -ef | grep hadoop

########## table: ##########

# ansible
# docker
# packer
# terraform

# 2181 Zookeeper

# 8000 DEBUG
# 8080 Tomcat

# 8081 Attendant app & /hallo OR /hi
# 8082 Visitor app & /hello OR /hi
# 8083 AwesomeConcepts app & /hillo OR /hi
# 8084 Eastworld app & /hollo OR /hi
# 8085 Signs app & /hullo OR /hi
# 8086 Optimizer app & /hyllo OR /hi
# 8087 SimulationScene app & /hllo OR /hi

# 8200 Vault
# 8500 Consul
# 8042 Hadoop1_3 !!!
# 8761 Eureka

# 9000 Hadoop1_1 !!!
# 9092 Kafka
# 9864 Hadoop1_4 !!!
# 9870 Hadoop1_2 !!!
