#!/bin/bash

## 04_03_docker-compose_up on ubuntu

echo "pwd =" $(pwd)
echo "USER_NAME_IN_OS = $USER_NAME_IN_OS, hostname =" $(hostname)
echo "date =" $(date)
echo "HOME = $HOME, USER = $USER, LOGNAME = $LOGNAME"
echo "By value: KAFKA_PORT = " $KAFKA_PORT ${KAFKA_PORT} "$KAFKA_PORT ${KAFKA_PORT}"
echo "By value: TOMCAT_VER = " $TOMCAT_VER ${TOMCAT_VER} "$TOMCAT_VER ${TOMCAT_VER}"

if [[ ! -z "$(cut -d= -f1 /home/$USER_NAME_IN_OS/.bashrc | grep declare)" ]]
then
	echo 'Environment variables are set.'
		echo "$(cut -d= -f1 /home/$USER_NAME_IN_OS/.bashrc | grep declare)"
fi

echo "%%%%%%%%%%%%%%%%%%"

pwd
cd $PROJECT_HOME
pwd
echo "RUNNING 04_03_docker-compose_up.sh"

############### Using docker-compose up ######################

sudo packer build -only=ubuntu-docker build.json
sudo docker-compose up

echo "&&&&&&&&&&&&&&&&&&&&01"




    ubuntu-docker: 76fdf21dc491: Verifying Checksum
    ubuntu-docker: 76fdf21dc491: Download complete
    ubuntu-docker: 76fdf21dc491: Pull complete
    ubuntu-docker: Digest: sha256:aed71132b5cf1facc84ed57c3206cc481156ec0ba6825750813d03e64e8819b6
    ubuntu-docker: Status: Downloaded newer image for gruntwork/ubuntu-test:16.04
    ubuntu-docker: docker.io/gruntwork/ubuntu-test:16.04
==> ubuntu-docker: Starting docker container...
    ubuntu-docker: Run command: docker run -v /home/kris/.packer.d/tmp/packer-docker563124196:/packer-files -d -i -t gruntwork/ubuntu-test:16.04 /bin/bash
    ubuntu-docker: Container ID: fe4d43086cdb2d24ab4ed532d2df743cfdf0e3aac3e4903613742b57106e0a1c
==> ubuntu-docker: Uploading /home/kris/KD/_KDPROJ/JAVA/_gitor/devel/simulationscene/_ubuntu_DO_ALL/packer-docker => /tmp/packer-docker-example
==> ubuntu-docker: Provisioning with shell script: /tmp/packer-shell206890989
    ubuntu-docker: /tmp/script_8126.sh: 2: /tmp/script_8126.sh: /tmp/packer-docker-example/configure-sinatra-app.sh: not found
==> ubuntu-docker: Killing the container: fe4d43086cdb2d24ab4ed532d2df743cfdf0e3aac3e4903613742b57106e0a1c
Build 'ubuntu-docker' errored: Script exited with non-zero exit status: 127

==> Some builds didn't complete successfully and had errors:
--> ubuntu-docker: Script exited with non-zero exit status: 127

==> Builds finished but no artifacts were created.
kris@gandalf1:~/KD/_KDPROJ/JAVA/_gitor/devel/simulationscene/_ubuntu_DO_ALL/packer-docker$ 

echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo "********************************************************************************"
