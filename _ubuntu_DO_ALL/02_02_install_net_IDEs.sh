#!/bin/bash

# 02_02_install_net_IDEs on ubuntu

echo "pwd =" $(pwd)
echo "USER_NAME_IN_OS = $USER_NAME_IN_OS, hostname =" $(hostname)
echo "date =" $(date)
echo "HOME = $HOME, USER = $USER, LOGNAME = $LOGNAME"
echo "By value: KAFKA_PORT = " $KAFKA_PORT ${KAFKA_PORT} "$KAFKA_PORT ${KAFKA_PORT}"
echo "By value: TOMCAT_VER = " $TOMCAT_VER ${TOMCAT_VER} "$TOMCAT_VER ${TOMCAT_VER}"

if [[ ! -z "$(cut -d= -f1 /home/$USER_NAME_IN_OS/.bashrc | grep declare)" ]]
then
	echo 'Environment variables are set.'
		echo "$(cut -d= -f1 /home/$USER_NAME_IN_OS/.bashrc | grep declare)"
fi

echo "%%%%%%%%%%%%%%%%%%"

pwd
cd $PROJECT_HOME
pwd
echo "RUNNING 02_02_install_net_IDEs.sh"

echo "apt install -y net-tools"
apt install -y net-tools
echo "ifconfig"
ifconfig
echo "apt install -y ping"
apt install -y ping
##or## apt-get install -y ping
echo "apt install -y iputils-ping"
apt install -y iputils-ping
##or## apt install -y inetutils-ping
echo "apt install -y telnet"
apt install -y telnet
##or## apt-get install -y telnet

echo "ping 192.168.1.22"
echo "ping 192.168.1.20"
#ping 192.168.1.22
## telnet 192.168.1.22 22
## telnet 192.168.1.22 13531 

echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"

echo "apt install -y netbeans"
apt install -y netbeans
# sudo apt install -y netbeans

echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
## Install it from the Software Center [Recommended]   https://itsfoss.com/install-intellij-ubuntu-linux/
## Installing IntelliJ IDEA is available in Ubuntu Software Center
##or##
echo "snap install intellij-idea-community --classic"
snap install intellij-idea-community --classic
# sudo snap install intellij-idea-community --classic

echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo "snap install --classic eclipse"
snap install --classic eclipse
# sudo snap install --classic eclipse

echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
## Visual Studio Code
# https://code.visualstudio.com/Download

su - $USER_NAME_IN_OS
echo "Choose the version at https://code.visualstudio.com/Download"
source /usr/bin/firefox -private -new-window "https://code.visualstudio.com/docs/?dv=linux64_deb"

echo "Pay attention!! Now the browser is opened with a .deb file to download and install..."
## Pay attention!! Now the browser is opened with a .deb file to download and install...

echo "Open the Browser and Choose the version at https://code.visualstudio.com/Download" >> $PROJECT_HOME/_ubuntu_DO_ALL/08_02_sudo_kubeadm_join.sh

exit 

echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo "********************************************************************************"
