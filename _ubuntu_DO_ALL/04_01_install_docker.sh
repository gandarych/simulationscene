#!/bin/bash

## 04_01_install_docker on ubuntu

echo "pwd =" $(pwd)
echo "USER_NAME_IN_OS = $USER_NAME_IN_OS, hostname =" $(hostname)
echo "date =" $(date)
echo "HOME = $HOME, USER = $USER, LOGNAME = $LOGNAME"
echo "By value: KAFKA_PORT = " $KAFKA_PORT ${KAFKA_PORT} "$KAFKA_PORT ${KAFKA_PORT}"
echo "By value: TOMCAT_VER = " $TOMCAT_VER ${TOMCAT_VER} "$TOMCAT_VER ${TOMCAT_VER}"

if [[ ! -z "$(cut -d= -f1 /home/$USER_NAME_IN_OS/.bashrc | grep declare)" ]]
then
	echo 'Environment variables are set.'
		echo "$(cut -d= -f1 /home/$USER_NAME_IN_OS/.bashrc | grep declare)"
fi

echo "%%%%%%%%%%%%%%%%%%"

pwd
cd $PROJECT_HOME
pwd
echo "RUNNING 04_01_install_docker.sh"

## add-apt-repository -y = silent execution of this command
## >> /tmp/output.txt
## The will tell Bash to redirect stderr to stdout.
# sudo add-apt-repository -y ppa:fossproject/ppa >> /tmp/output.txt
## Or use output redirection to /dev/null and include stderr output.
# sudo add-apt-repository -y ppa:fossproject/ppa >> /dev/null

echo "############### Installing DOCKER ######################"

# Try to install the DOCKER:
echo "sudo apt install -y docker.io"
sudo apt install -y docker.io
echo "sudo docker"
sudo docker
echo "sudo docker --version"
sudo docker --version
## see more: https://tecadmin.net/tutorial/docker/docker-tutorials/

echo "&&&&&&&&&&&&&&&&&&&&01"
# If something is missing or not, next let's try to use advices of Brian Hogan at his article:
# on https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-18-04

# Updating the list of packages:
echo "sudo apt update"
sudo apt update
### >> enter the password

# Installing some prerequisite packages which let apt use packages over HTTPS:
echo "sudo apt install -y apt-transport-https ca-certificates curl software-properties-common"
sudo apt install -y apt-transport-https ca-certificates curl software-properties-common

# Adding the GPG key for the official Docker repository to your system:
echo "curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -"
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

# Adding the Docker repository to APT sources:
echo 'sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"'
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"

# Updating the list of packages database with Docker from the newly added repo:
echo "sudo apt update"
sudo apt update

echo "&&&&&&&&&&&&&&&&&&&&02"
# Making sure we are about to install from the Docker repo (instead of the default Ubuntu repo):
echo "sudo apt-cache policy docker-ce"
sudo apt-cache policy docker-ce

# Installing Docker - Container Engine:
echo "sudo apt install -y docker-ce"
sudo apt install -y docker-ce

echo "&&&&&&&&&&&&&&&&&&&&03"
# Checking: Docker should now be installed, the daemon started, and the process enabled to start on boot. Check that it's running:
echo "sudo systemctl status docker"
echo 'tail -f /home/${USER_NAME_IN_OS}/KD/_KDPROJ/tmp/LOGS/docker_active.log' >> ./_ubuntu_DO_ALL/08_02_sudo_kubeadm_join.sh
sudo systemctl status docker > '/home/${USER_NAME_IN_OS}/KD/_KDPROJ/tmp/LOGS/docker_active.log'

## OUTPUT:
####  *  docker.service - Docker Application Container Engine
####    Loaded: loaded (/lib/systemd/system/docker.service; enabled; vendor preset: enabled)
####    Active: active (running) since Sun RRRR-MM-DD 15:05:33 CET; 5s ago
####      Docs: https://docs.docker.com
####   Main PID: 20373 (dockerd)
####     Tasks: 12
####    CGroup: /system.slice/docker.service
####             └─20373 /usr/bin/dockerd -H fd:// --containerd=/run/containerd/containerd.sock

echo "sudo apt install docker-compose"
sudo apt install -y docker-compose
docker-compose -v


echo "############### Executing the DOCKER Command Without Sudo ######################"
echo "&&&&&&&&&&&&&&&&&&&&04"
echo "sudo docker run --help"
sudo docker run --help

# If you want to avoid typing "sudo" whenever you run the docker command, add your username to the docker group:
echo "echo ${USER_NAME_IN_OS}"
echo ${USER_NAME_IN_OS}
echo "sudo usermod -aG docker ${USER_NAME_IN_OS}"
sudo usermod -aG docker ${USER_NAME_IN_OS}

# Apllying the new group membership. Type the following:
echo "su - ${USER_NAME_IN_OS}.   Type 'exit' !!!!!!!!!!!!"
su - ${USER_NAME_IN_OS}

# !!!!!!!!!!!!!!!!!!!!!!!!!!!!
#Type 'exit' to exit from "su -" !!!!!!!!!!!!!!!!!!!!!!!!!!!!

# Checking the confirmation that the user is now added to the docker group by typing:
echo "id -nG"
id -nG

# Sample output:   kris adm cdrom sudo dip plugdev lpadmin sambashare docker


## (Optional) If you need to add a user to the docker group that you're not logged in as, declare that username explicitly using:
##   sudo usermod -aG docker username

echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo "********************************************************************************"
