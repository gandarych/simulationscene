#!/bin/bash

## 01_06_add_screen_recorder.sh on ubuntu

echo "pwd =" $(pwd)
echo "USER_NAME_IN_OS = $USER_NAME_IN_OS, hostname =" $(hostname)
echo "date =" $(date)
echo "HOME = $HOME, USER = $USER, LOGNAME = $LOGNAME"
echo "By value: KAFKA_PORT = " $KAFKA_PORT ${KAFKA_PORT} "$KAFKA_PORT ${KAFKA_PORT}"
echo "By value: TOMCAT_VER = " $TOMCAT_VER ${TOMCAT_VER} "$TOMCAT_VER ${TOMCAT_VER}"

if [[ ! -z "$(cut -d= -f1 /home/$USER_NAME_IN_OS/.bashrc | grep declare)" ]]
then
	echo 'Environment variables are set.'
		echo "$(cut -d= -f1 /home/$USER_NAME_IN_OS/.bashrc | grep declare)"
fi

echo "%%%%%%%%%%%%%%%%%%"

pwd
cd $PROJECT_HOME
pwd
echo "RUNNING 01_06_add_screen_recorder.sh"

## add-apt-repository -y = silent execution of this command
## >> /tmp/output.txt 2>&1
## The 2>&1 will tell Bash to redirect stderr to stdout.
# sudo add-apt-repository -y ppa:fossproject/ppa >> /tmp/output.txt 2>&1
## Or use output redirection to /dev/null and include stderr output.
# sudo add-apt-repository -y ppa:fossproject/ppa >> /dev/null 2>&1


# https://itsfoss.com/best-linux-screen-recorders/

# Minimal:
sudo apt install -y kazam
kazam -v

echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
# Medium - 1:
sudo apt install -y vokoscreen

echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
# Better medium - 2 (more formats):
sudo add-apt-repository -y ppa:fossproject/ppa
sudo apt update
sudo apt install -y green-recorder

echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
# Very useful:
sudo add-apt-repository -y ppa:obsproject/obs-studio
sudo apt update
sudo apt install -y obs-studio
obs --version
obs --help

echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
# Innovative: transparent frame - sufficient in many times!
sudo add-apt-repository -y ppa:peek-developers/stable
sudo apt update
sudo apt install -y peek
peek -v

echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
# video/mp4/avi editor:
snap install shotcut --classic

echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
# audio/mp3 editor:

sudo add-apt-repository -y ppa:ubuntuhandbook1/audacity

sudo apt update

sudo apt install -y audacity

sudo dpkg --configure -a

sudo apt install -y audacity

echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"

# https://alternativeto.net/software/simplescreenrecorder/

# https://alternativeto.net/software/open-broadcaster-software/

# https://community.udemy.com/t5/First-time-Course-Creation/Linux-alternative-to-Camtasia/td-p/14451

# https://www.slant.co/options/8066/alternatives/~camtasia-alternatives

echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo "********************************************************************************"
