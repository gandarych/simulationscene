#!/bin/bash

## Setting environment variables:
echo "pwd =" $(pwd)
echo "USER_NAME_IN_OS = $USER_NAME_IN_OS, hostname =" $(hostname)
echo "date =" $(date)
echo "HOME = $HOME, USER = $USER, LOGNAME = $LOGNAME"
echo "By value: KAFKA_PORT = " $KAFKA_PORT ${KAFKA_PORT} "$KAFKA_PORT ${KAFKA_PORT}"
echo "By value: TOMCAT_VER = " $TOMCAT_VER ${TOMCAT_VER} "$TOMCAT_VER ${TOMCAT_VER}"

if [[ ! -z "$(cut -d= -f1 /home/$USER_NAME_IN_OS/.bashrc | grep declare)" ]]
then
	echo 'Environment variables are set.'
		echo "$(cut -d= -f1 /home/$USER_NAME_IN_OS/.bashrc | grep declare)"
fi

echo "%%%%%%%%%%%%%%%%%%"

pwd
cd $PROJECT_HOME
pwd

echo "----------------------- # 8081 Attendant app & /hallo OR /hi # ----------------------------"
gnome-terminal &
cd $PROJECT_HOME/../attendant/
  if [[ $MAVEN_ON_AND_GRADLE_OFF == "Y" ]]
  then
    source jsilently_MVN_build_all_deploy_and_run_EN.sh
  fi
  if [[ $MAVEN_ON_AND_GRADLE_OFF == "N" ]]
  then
    source ksilently_GRADLE_build_all_deploy_and_run_EN.sh
  fi
sudo netstat -ao | grep -i "8081"

echo "----------------------- # 8082 Visitor app & /hello OR /hi # ----------------------------"
gnome-terminal &
cd $PROJECT_HOME/../visitor/
  if [[ $MAVEN_ON_AND_GRADLE_OFF == "Y" ]]
  then
    source jsilently_MVN_build_all_deploy_and_run_EN.sh
  fi
  if [[ $MAVEN_ON_AND_GRADLE_OFF == "N" ]]
  then
    source ksilently_GRADLE_build_all_deploy_and_run_EN.sh
  fi
sudo netstat -ao | grep -i "8082" 

echo "----------------------- # 8083 AwesomeConcepts app & /hillo OR /hi # ----------------------------"
gnome-terminal &
cd $PROJECT_HOME/../awesomeconcepts/
  if [[ $MAVEN_ON_AND_GRADLE_OFF == "Y" ]]
  then
    source jsilently_MVN_build_all_deploy_and_run_EN.sh
  fi
  if [[ $MAVEN_ON_AND_GRADLE_OFF == "N" ]]
  then
    source ksilently_GRADLE_build_all_deploy_and_run_EN.sh
  fi
sudo netstat -ao | grep -i "8083" 

echo "----------------------- # 8084 Eastworld app & /hollo OR /hi # ----------------------------"
gnome-terminal &
cd $PROJECT_HOME/../eastworld/
  if [[ $MAVEN_ON_AND_GRADLE_OFF == "Y" ]]
  then
    source jsilently_MVN_build_all_deploy_and_run_EN.sh
  fi
  if [[ $MAVEN_ON_AND_GRADLE_OFF == "N" ]]
  then
    source ksilently_GRADLE_build_all_deploy_and_run_EN.sh
  fi
sudo netstat -ao | grep -i "8084" 

echo "----------------------- # 8085 Signs app & /hullo OR /hi # ----------------------------"
gnome-terminal &
cd $PROJECT_HOME/../signs/
  if [[ $MAVEN_ON_AND_GRADLE_OFF == "Y" ]]
  then
    source jsilently_MVN_build_all_deploy_and_run_EN.sh
  fi
  if [[ $MAVEN_ON_AND_GRADLE_OFF == "N" ]]
  then
    source ksilently_GRADLE_build_all_deploy_and_run_EN.sh
  fi
sudo netstat -ao | grep -i "8085" 

echo "----------------------- # 8086 Optimizer app & /hyllo OR /hi # ----------------------------"
gnome-terminal &
cd $PROJECT_HOME/../optimizer/
  if [[ $MAVEN_ON_AND_GRADLE_OFF == "Y" ]]
  then
    source jsilently_MVN_build_all_deploy_and_run_EN.sh
  fi
  if [[ $MAVEN_ON_AND_GRADLE_OFF == "N" ]]
  then
    source ksilently_GRADLE_build_all_deploy_and_run_EN.sh
  fi
sudo netstat -ao | grep -i "8086" 

echo "----------------------- # 8087 SimulationScene app & /hllo OR /hi # ----------------------------"
gnome-terminal &
cd $PROJECT_HOME/../simulationscene/
  if [[ $MAVEN_ON_AND_GRADLE_OFF == "Y" ]]
  then
    source jsilently_MVN_build_all_deploy_and_run_EN.sh
  fi
  if [[ $MAVEN_ON_AND_GRADLE_OFF == "N" ]]
  then
    source ksilently_GRADLE_build_all_deploy_and_run_EN.sh
  fi
sudo netstat -ao | grep -i "8087"




echo "################### CHECK WHAT IS STARTED NOW ###################"
source ./96_01_check_started_services_now.sh


