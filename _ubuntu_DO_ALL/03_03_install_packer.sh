#!/bin/bash

###### 03_03_install_packer on ubuntu

echo "pwd =" $(pwd)
echo "USER_NAME_IN_OS = $USER_NAME_IN_OS, hostname =" $(hostname)
echo "date =" $(date)
echo "HOME = $HOME, USER = $USER, LOGNAME = $LOGNAME"
echo "By value: KAFKA_PORT = " $KAFKA_PORT ${KAFKA_PORT} "$KAFKA_PORT ${KAFKA_PORT}"
echo "By value: TOMCAT_VER = " $TOMCAT_VER ${TOMCAT_VER} "$TOMCAT_VER ${TOMCAT_VER}"

if [[ ! -z "$(cut -d= -f1 /home/$USER_NAME_IN_OS/.bashrc | grep declare)" ]]
then
	echo 'Environment variables are set.'
		echo "$(cut -d= -f1 /home/$USER_NAME_IN_OS/.bashrc | grep declare)"
fi

echo "%%%%%%%%%%%%%%%%%%"

pwd
cd $PROJECT_HOME
pwd
echo "RUNNING 03_03_install_packer.sh"

echo "apt update"
apt update

echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
# Check the latest release of Packer on the https://www.packer.io/downloads.html page. 
# Then download the recent version for your platform.
cd /tmp
wget https://releases.hashicorp.com/packer/$PACKER_VER/packer_$PACKER_VER_linux_amd64.zip
unzip packer_$PACKER_VER_linux_amd64.zip
sudo mv packer /usr/local/bin
# Verify the installation is working by checking that the packer is available:
packer 

echo "apt install -y packer"
apt install -y packer
## apt install pip
## pip install packer
echo "packer --version"
packer --version
echo "packer -v"
packer -v

## OUTPUT:
####  1.0.4


####   https://github.com/startup-systems/terraform-ansible-example
####   https://www.hashicorp.com/resources/ansible-terraform-better-together
####   https://alex.dzyoba.com/blog/terraform-ansible/
####   https://medium.com/faun/building-repeatable-infrastructure-with-terraform-and-ansible-on-aws-3f082cd398ad
####   https://victorops.com/blog/writing-ansible-playbooks-for-new-terraform-servers
####   https://getintodevops.com/blog/using-ansible-with-terraform
####   https://www.redhat.com/cms/managed-files/pa-terraform-and-ansible-overview-f14774wg-201811-en.pdf
####   https://stackshare.io/stackups/ansible-vs-terraform    (LOGO, following)

####   Packer provisioner runs Ansible playbooks!!!:
####   https://packer.io/docs/provisioners/ansible.html

echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo "********************************************************************************"
