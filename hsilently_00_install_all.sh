#!/bin/bash



## https://bash.cyberciti.biz/guide/Variables
#set -o allexport
#[[ -f ./_ubuntu_DO_ALL/ENV_VAR.txt ]] && . ./_ubuntu_DO_ALL/ENV_VAR.txt
#set +o allexport

set -a
[ -f ./_ubuntu_DO_ALL/ENV_VAR.txt ] && source ./_ubuntu_DO_ALL/ENV_VAR.txt
set +a


source ./_ubuntu_DO_ALL/ENV_VAR.txt
## 'cut' command: cuts/splits lines on the delimeter (specified by -d) 
## and then selects certain fields from those cut up lines. 
## Which fields, is specified by -f (counting starts at 1, not at 0). 
## See 'man cut': -s, --only-delimited = do not print lines not containing delimiters
export $(cut -d= -f1 ./_ubuntu_DO_ALL/ENV_VAR.txt)
export $(cut -d= -f1 ./_ubuntu_DO_ALL/WHAT_TO_INSTALL.txt)

sudo echo "%%%%%%%%% 12 %%%%%%%%%"
sudo echo "sudo USER_NAME_IN_OS = $USER_NAME_IN_OS"
sudo echo "USER_NAME_IN_OS = $USER_NAME_IN_OS"

sudo echo "%%%%%%%%% 13 %%%%%%%%%"
if [[ -z "$USER_NAME_IN_OS" ]] 
then
	echo 'ERROR! Failed! Variable $USER_NAME_IN_OS is not set!'
fi
if [[ ! -z "$USER_NAME_IN_OS" ]] 
then
	echo "USER_NAME_IN_OS = $USER_NAME_IN_OS"
	if [[ ! -z "$(cut -d= -f1 /home/$USER_NAME_IN_OS/.bashrc | grep declare)" ]]
	then
		echo 'Environment variables are set.'
		echo "$(cut -d= -f1 /home/$USER_NAME_IN_OS/.bashrc | grep declare)"
	fi
	if [[ -z "$(cut -d= -f1 /home/$USER_NAME_IN_OS/.bashrc | grep declare)" ]] 
	then
		echo 'Setting up!!! Environment variables:'
		sudo echo "$(export | grep -v root | grep -v COLORTERM | grep -v PWD= | sort)" 
		sudo echo "$(export | grep -v root | grep -v COLORTERM | grep -v PWD= | sort)" >> /home/$USER_NAME_IN_OS/.bashrc
	fi
fi

source $PROJECT_HOME/_ubuntu_DO_ALL/00_01_EnvVAR_check_new_versions.sh

source $PROJECT_HOME/_ubuntu_DO_ALL/01_01_update_ubuntu.sh
# cat $PROJECT_HOME/_ubuntu_DO_ALL/01_02_bitbucket_public_projects.txt
# cat $PROJECT_HOME/_ubuntu_DO_ALL/01_03_bitbucket_projects.txt
source $PROJECT_HOME/_ubuntu_DO_ALL/01_04_install_openjdk.sh
source $PROJECT_HOME/_ubuntu_DO_ALL/01_05_install_htop_gparted_mc.sh
source $PROJECT_HOME/_ubuntu_DO_ALL/01_06_add_screen_recorder.sh
source $PROJECT_HOME/_ubuntu_DO_ALL/02_01_install_maven_gradle.sh
source $PROJECT_HOME/_ubuntu_DO_ALL/02_02_install_net_IDEs.sh
source $PROJECT_HOME/_ubuntu_DO_ALL/02_03_install_tomcat.sh
source $PROJECT_HOME/_ubuntu_DO_ALL/02_04_add_tomcat_user.sh
source $PROJECT_HOME/_ubuntu_DO_ALL/02_05_install_discovery_kafka.sh
# source $PROJECT_HOME/_ubuntu_DO_ALL/02_06_configure_eureka.txt
source $PROJECT_HOME/_ubuntu_DO_ALL/02_07_install_secrets.sh
source $PROJECT_HOME/_ubuntu_DO_ALL/02_08_install_hadoop.sh

if [ "$YES_TO_INSTALL_ANSIBLE" == "Y" ]
then
	
fi
if [ "$YES_TO_INSTALL_CONSUL" == "Y" ]
then
	
fi
if [ "$YES_TO_INSTALL_DOCKERCOMPOSE" == "Y" ]
then
	
fi
if [ "$YES_TO_INSTALL_DOCKER" == "Y" ]
then
	
fi
if [ "$YES_TO_INSTALL_EUREKA" == "Y" ]
then
	
fi
if [ "$YES_TO_INSTALL_GOLANG" == "Y" ]
then
	
fi
if [ "$YES_TO_INSTALL_GRADLE" == "Y" ]
then
	
fi
if [ "$YES_TO_INSTALL_HADOOP" == "Y" ]
then
	
fi
if [ "$YES_TO_INSTALL_HCL" == "Y" ]
then
	
fi
if [ "$YES_TO_INSTALL_HTOP_GPARTED" == "Y" ]
then
	
fi
if [ "$YES_TO_INSTALL_KAFKA" == "Y" ]
then
	
fi
if [ "$YES_TO_INSTALL_KUBERNETES" == "Y" ]
then
	
fi
if [ "$YES_TO_INSTALL_MAVEN" == "Y" ]
then
	
fi
if [ "$YES_TO_INSTALL_NET_IDES" == "Y" ]
then
	
fi
if [ "$YES_TO_INSTALL_NODEJS" == "Y" ]
then
	
fi
if [ "$YES_TO_INSTALL_PACKER" == "Y" ]
then
	
fi
if [ "$YES_TO_INSTALL_RUBY_SINATRA" == "Y" ]
then
	
fi
if [ "$YES_TO_INSTALL_SCREEN_RECORDERS" == "Y" ]
then
	
fi
if [ "$YES_TO_INSTALL_TERRAFORM" == "Y" ]
then
	
fi
if [ "$YES_TO_INSTALL_TOMCAT" == "Y" ]
then
	
fi
if [ "$YES_TO_INSTALL_VAULT" == "Y" ]
then
	
fi
if [ "$YES_TO_INSTALL_ZOOKEEPER" == "Y" ]
then
	
fi

# source $PROJECT_HOME/_ubuntu_DO_ALL/02_09_configure_hadoop.txt
source $PROJECT_HOME/_ubuntu_DO_ALL/03_01_vi_tomcat_service.sh
source $PROJECT_HOME/_ubuntu_DO_ALL/03_02_systemctl_start_tomcat.sh
source $PROJECT_HOME/_ubuntu_DO_ALL/03_03_install_packer.sh
source $PROJECT_HOME/_ubuntu_DO_ALL/03_04_ruby_sinatra_nodejs.sh
source $PROJECT_HOME/_ubuntu_DO_ALL/03_05_install_Golang.sh
source $PROJECT_HOME/_ubuntu_DO_ALL/04_01_install_docker.sh
source $PROJECT_HOME/_ubuntu_DO_ALL/04_02_using_docker_cmd_AS_ROOT.sh
# source $PROJECT_HOME/_ubuntu_DO_ALL/04_03_docker-compose_up.sh
source $PROJECT_HOME/_ubuntu_DO_ALL/05_01_install_ansible_AS_ROOT.sh
source $PROJECT_HOME/_ubuntu_DO_ALL/06_01_install_terraform_AS_ROOT.sh
source $PROJECT_HOME/_ubuntu_DO_ALL/07_01_hcl_configuration.sh
source $PROJECT_HOME/_ubuntu_DO_ALL/08_01_install_kubernetes.sh

# source $PROJECT_HOME/_ubuntu_DO_ALL/08_02_sudo_kubeadm_join.sh
echo "# source $PROJECT_HOME/_ubuntu_DO_ALL/08_02_sudo_kubeadm_join.sh"

source $PROJECT_HOME/_ubuntu_DO_ALL/96_01_check_started_services_now.sh

# source $PROJECT_HOME/_ubuntu_DO_ALL/97_01_start_services_before_build.sh

# source $PROJECT_HOME/_ubuntu_DO_ALL/98_01_build_and_start_projects.sh
### source jsilently_MVN_build_all_deploy_and_run_EN.sh
### source ksilently_GRADLE_build_all_deploy_and_run_EN.sh

