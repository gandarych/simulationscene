#!/bin/bash

## 03_01_vi_tomcat_service on ubuntu

echo "pwd =" $(pwd)
echo "USER_NAME_IN_OS = $USER_NAME_IN_OS, hostname =" $(hostname)
echo "date =" $(date)
echo "HOME = $HOME, USER = $USER, LOGNAME = $LOGNAME"
echo "By value: KAFKA_PORT = " $KAFKA_PORT ${KAFKA_PORT} "$KAFKA_PORT ${KAFKA_PORT}"
echo "By value: TOMCAT_VER = " $TOMCAT_VER ${TOMCAT_VER} "$TOMCAT_VER ${TOMCAT_VER}"

if [[ ! -z "$(cut -d= -f1 /home/$USER_NAME_IN_OS/.bashrc | grep declare)" ]]
then
	echo 'Environment variables are set.'
		echo "$(cut -d= -f1 /home/$USER_NAME_IN_OS/.bashrc | grep declare)"
fi

echo "%%%%%%%%%%%%%%%%%%"

pwd
cd $PROJECT_HOME
pwd
echo "RUNNING 03_01_vi_tomcat_service.sh"


## create a Systemd Service File
## Copy the path of Tomcat’s home by running this command:
# sudo update-java-alternatives -l
## Output:
## java-1.8.0-openjdk-amd64       1081       /usr/lib/jvm/java-1.8.0-openjdk-amd64
##or##
## java-1.11.0-openjdk-amd64      1101       /usr/lib/jvm/java-1.11.0-openjdk-amd64
## take the highlighted path and put it into your /etc/systemd/system/tomcat.service file, as the JAVA_HOME variable (shown below).
#vi /etc/systemd/system/tomcat.service
## Create a unit file to run Tomcat as a service: for startup.sh and shutdown.sh files are located in the /bin directory of Tomcat.
## Be sure that your paths are the correct paths (:wq)
#[Unit]
#Description=Tomcat 9 servlet container
#After=network.target
## After=syslog.target network.target
#[Service]
#Type=forking
#User=tomcat
#Group=tomcat
#Environment="JAVA_HOME=/usr/lib/jvm/java-${JAVA_VER}-openjdk-amd64/"
#Environment="JAVA_OPTS=-Djava.security.egd=file:///dev/urandom -Djava.awt.headless=true"
#Environment="CATALINA_BASE=/opt/tomcat/apache-tomcat-${TOMCAT_VER}"
#Environment="CATALINA_HOME=/opt/tomcat/apache-tomcat-${TOMCAT_VER}"
#Environment="CATALINA_PID=/opt/tomcat/apache-tomcat-${TOMCAT_VER}/temp/tomcat.pid"
#Environment="CATALINA_OPTS=-Xms512M -Xmx1024M -server -XX:+UseParallelGC"
#WorkingDirectory=/opt/tomcat/apache-tomcat-${TOMCAT_VER}
#ExecStart=/opt/tomcat/apache-tomcat-${TOMCAT_VER}/bin/startup.sh
#ExecStop=/opt/tomcat/apache-tomcat-${TOMCAT_VER}/bin/shutdown.sh
## UMask=0007
## RestartSec=10
## Restart=always
#[Install]
#WantedBy=multi-user.target


echo "cd /etc/systemd/system/"
cd /etc/systemd/system/
echo "touch tomcat.service"
touch tomcat.service
echo "[Unit]" > tomcat.service
echo "Description=Tomcat 9 servlet container" >> tomcat.service
echo "After=network.target" >> tomcat.service
echo "# After=syslog.target network.target" >> tomcat.service
echo "[Service]" >> tomcat.service
echo "Type=forking" >> tomcat.service
echo "User=tomcat" >> tomcat.service
echo "Group=tomcat" >> tomcat.service
echo "Environment=JAVA_HOME=/usr/lib/jvm/java-$JAVA_VER-openjdk-amd64/" >> tomcat.service
echo 'Environment="JAVA_OPTS=-Djava.security.egd=file:///dev/urandom -Djava.awt.headless=true"' >> tomcat.service
echo "Environment=CATALINA_BASE=/opt/tomcat/apache-tomcat-$TOMCAT_VER" >> tomcat.service
echo "Environment=CATALINA_HOME=/opt/tomcat/apache-tomcat-$TOMCAT_VER" >> tomcat.service
echo "Environment=CATALINA_PID=/opt/tomcat/apache-tomcat-$TOMCAT_VER/temp/tomcat.pid" >> tomcat.service
echo "Environment='CATALINA_OPTS=-Xms512M -Xmx1024M -server -XX:+UseParallelGC'"
echo "WorkingDirectory=/opt/tomcat/apache-tomcat-$TOMCAT_VER" >> tomcat.service
echo "ExecStart=/opt/tomcat/apache-tomcat-$TOMCAT_VER/bin/startup.sh" >> tomcat.service
echo "ExecStop=/opt/tomcat/apache-tomcat-$TOMCAT_VER/bin/shutdown.sh" >> tomcat.service
echo '# UMask=0007' >> tomcat.service
echo '# RestartSec=10' >> tomcat.service
echo '# Restart=always' >> tomcat.service
echo '[Install]' >> tomcat.service
echo 'WantedBy=multi-user.target' >> tomcat.service
echo "cat tomcat.service"
cat tomcat.service

echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo "********************************************************************************"
