#!/bin/bash

# 02_01_install_maven_gradle on ubuntu

echo "pwd =" $(pwd)
echo "USER_NAME_IN_OS = $USER_NAME_IN_OS, hostname =" $(hostname)
echo "date =" $(date)
echo "HOME = $HOME, USER = $USER, LOGNAME = $LOGNAME"
echo "By value: KAFKA_PORT = " $KAFKA_PORT ${KAFKA_PORT} "$KAFKA_PORT ${KAFKA_PORT}"
echo "By value: TOMCAT_VER = " $TOMCAT_VER ${TOMCAT_VER} "$TOMCAT_VER ${TOMCAT_VER}"

if [[ ! -z "$(cut -d= -f1 /home/$USER_NAME_IN_OS/.bashrc | grep declare)" ]]
then
	echo 'Environment variables are set.'
		echo "$(cut -d= -f1 /home/$USER_NAME_IN_OS/.bashrc | grep declare)"
fi

echo "%%%%%%%%%%%%%%%%%%"

pwd
cd $PROJECT_HOME
pwd
echo "RUNNING 02_01_install_maven_gradle.sh"

# remove old if exists:
echo "cd /opt/"
cd /opt/
echo "sudo rm -r /opt/maven-${MAVEN_VER}/   CLEANING"
sudo rm -r /opt/maven-${MAVEN_VER}/
# download the latest stable version of Apache Maven from the official website:
echo "sudo wget https://www-us.apache.org/dist/maven/maven-3/${MAVEN_VER}/binaries/apache-maven-${MAVEN_VER}-bin.tar.gz"
sudo wget https://www-us.apache.org/dist/maven/maven-3/${MAVEN_VER}/binaries/apache-maven-${MAVEN_VER}-bin.tar.gz
# extract the downloaded archive:
echo "sudo tar -xvzf apache-maven-${MAVEN_VER}-bin.tar.gz"
sudo tar -xvzf apache-maven-${MAVEN_VER}-bin.tar.gz
# rename the extracted directory:
echo "sudo mkdir /opt/maven/"
sudo mkdir /opt/maven/
echo "sudo mv /opt/apache-maven-${MAVEN_VER} /opt/maven/maven-${MAVEN_VER}"
sudo mv /opt/apache-maven-${MAVEN_VER} /opt/maven/maven-${MAVEN_VER}
sudo cp -pr /tmp/maven_conf/settings.xml /opt/maven/maven-${MAVEN_VER}/conf
echo "cd /opt/maven/maven-${MAVEN_VER}/conf"
cd /opt/maven/maven-${MAVEN_VER}/conf
sudo ls -al

echo "sudo rm /opt/apache-maven-${MAVEN_VER}-bin.tar.gz"
sudo rm /opt/apache-maven-${MAVEN_VER}-bin.tar.gz

echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"

echo "sudo apt install -y gradle"
sudo apt install -y gradle
gradle -v

echo "/usr/bin/gradle -v"
/usr/bin/gradle -v
##or##

echo "sudo apt install -y wget unzip"
sudo apt install -y wget unzip

# latest version from https://services.gradle.org/distributions/*
cd /tmp/
wget https://services.gradle.org/distributions/gradle-${GRADLE_VER}-bin.zip

unzip gradle-*.zip

echo "sudo rm -r /opt/gradle   CLEANING"
sudo rm -r /opt/gradle
sudo mkdir /opt/gradle

sudo cp -pr /tmp/gradle-*/* /opt/gradle

echo "sudo rm -r /tmp/gradle-*.zip   CLEANING"
sudo rm -r /tmp/gradle-*.zip
echo "sudo rm -r /tmp/gradle-*   CLEANING"
sudo rm -r /tmp/gradle-*

sudo ls -al /opt/gradle

#Setting up environment variables

## Configure the path environment variable to include the Gradle’s bin directory. 
## For this create a new file inside the /etc/profile.d/ directory.

sudo touch /etc/profile.d/gradle.sh 

sudo echo "export PATH=$PATH:/opt/gradle/bin" >> /etc/profile.d/gradle.sh 

# sudo nano /etc/profile.d/gradle.sh 
export "PATH=$PATH:/opt/gradle/bin"

sudo chmod +x /etc/profile.d/gradle.sh
source /etc/profile.d/gradle.sh

sudo echo "PATH=$PATH:/opt/gradle/bin" >> ~/.bashrc
source ~/.bashrc

echo "/opt/gradle/bin/gradle -v" 
cd /opt/gradle/bin
./gradle -v

echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo "apt-get install curl"
sudo apt-get install curl

echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo "********************************************************************************"
