#!/bin/bash

## 02_05_install_discovery.sh on ubuntu

echo "pwd =" $(pwd)
echo "USER_NAME_IN_OS = $USER_NAME_IN_OS, hostname =" $(hostname)
echo "date =" $(date)
echo "HOME = $HOME, USER = $USER, LOGNAME = $LOGNAME"
echo "By value: KAFKA_PORT = " $KAFKA_PORT ${KAFKA_PORT} "$KAFKA_PORT ${KAFKA_PORT}"
echo "By value: TOMCAT_VER = " $TOMCAT_VER ${TOMCAT_VER} "$TOMCAT_VER ${TOMCAT_VER}"

if [[ ! -z "$(cut -d= -f1 /home/$USER_NAME_IN_OS/.bashrc | grep declare)" ]]
then
	echo 'Environment variables are set.'
		echo "$(cut -d= -f1 /home/$USER_NAME_IN_OS/.bashrc | grep declare)"
fi

echo "%%%%%%%%%%%%%%%%%%"

pwd
echo "${PROJECT_HOME}"
pwd
cd $PROJECT_HOME
pwd

## add-apt-repository -y = silent execution of this command
## >> /tmp/output.txt
## The will tell Bash to redirect stderr to stdout.
# sudo add-apt-repository -y ppa:fossproject/ppa >> /tmp/output.txt
## Or use output redirection to /dev/null and include stderr output.
# sudo add-apt-repository -y ppa:fossproject/ppa >> /dev/null
#To redirect stdout to one file and stderr to another file: command > out 2>error
# sudo add-apt-repository -y ppa:fossproject/ppa >> /tmp/output.txt 2> /tmp/error.txt
#To redirect both stderr and stdout (standard output): command &> output.txt
# sudo add-apt-repository -y ppa:fossproject/ppa &> /tmp/both_output.txt
## cat /etc/fstab

echo "RUNNING 02_05_install_discovery.sh"

## https://learn.hashicorp.com/consul/getting-started/agent
## https://phoenixnap.com/kb/how-to-install-vault-ubuntu
## https://www.consul.io/downloads.html
## copy link location

sudo apt install -y unzip
echo 'tail -f /home/${USER_NAME_IN_OS}/KD/_KDPROJ/tmp/LOGS/consul_active.log' | tee -a $PROJECT_HOME/_ubuntu_DO_ALL/08_02_sudo_kubeadm_join.sh
cd /tmp
pwd
wget https://releases.hashicorp.com/consul/${CONSUL_VER}/consul_${CONSUL_VER}_linux_amd64.zip
unzip consul_${CONSUL_VER}_linux_amd64.zip
sudo mv consul /usr/bin
export "PATH=$PATH:/usr/bin/consul"
sudo echo 'export PATH=$PATH:/usr/bin/consul' >> ~/.bashrc
consul -v
consul --help

echo "&&&&&&&&&&&&&&&&&&&&01"
pwd
cd $PROJECT_HOME
pwd
## RUN...
#consul agent -dev -node machine > "/home/${USER_NAME_IN_OS}/KD/_KDPROJ/tmp/LOGS/consul_active.log" 
echo "!!!!!!!!!!!!!!!!!!!!!!!"
echo 'consul agent -dev -node machine > "/home/${USER_NAME_IN_OS}/KD/_KDPROJ/tmp/LOGS/consul_active.log"' | tee -a $PROJECT_HOME/_ubuntu_DO_ALL/08_02_sudo_kubeadm_join.sh

echo "tail -f /home/${USER_NAME_IN_OS}/KD/_KDPROJ/tmp/LOGS/tomcat_active.log" | tee -a $PROJECT_HOME/_ubuntu_DO_ALL/08_02_sudo_kubeadm_join.sh
gnome-terminal &



echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
pwd
##OR#

## Installing Apache Zookeeper... CHECK THE LAST VERSION AT https://downloads.apache.org/zookeeper/ and http://zookeeper.apache.org/releases.html 
## How it works: https://zookeeper.apache.org/doc/r3.6.0/zookeeperOver.html
## Admin guide: https://zookeeper.apache.org/doc/r3.6.0/zookeeperAdmin.html
## http://zookeeper.apache.org/doc/current/zookeeperAdmin.html#sc_maintenance
### Clients connect to a single ZooKeeper server. 
### The client maintains a TCP connection through which it sends requests, 
### gets responses, gets watch events, and sends heart beats. 
### If the TCP connection to the server breaks, the client will connect to a different server.
### Each node has an Access Control List (ACL) that restricts who can do what.
### These znodes exists as long as the session that created the znode is active. When the session ends the znode is deleted.
## https://my-bigdata-blog.blogspot.com/2017/07/apache-Zookeeper-install-Ubuntu.html
## https://medium.com/@ryannel/installing-zookeeper-on-ubuntu-9f1f70f22e25
## https://askubuntu.com/questions/1022575/what-is-the-proper-way-to-install-zookeeper-on-ubuntu-16-04-for-both-standalone

##OR# sudo apt-get install -y zookeeperd ##OR#
pwd
echo "sudo useradd -r -m -U -d /opt/z1 -s /bin/false zookeeper"
## Creation a non-login user 'zookeeper':
sudo useradd -r -m -U -d /opt/z1 -s /bin/false zookeeper
##  -r, --system                        create a system account
##  -m, --create-home               create the user's home directory
##  -U, --user-group                  create a group with the same name as the user
##  -d, --home-dir HOME_DIR   home directory of the new account
##  -s, --shell SHELL                login shell of the new account
##  -g, --gid GROUP                 name or ID of the primary group of the new account, if not the same name as the user

sudo usermod -a -G "$MISE_GROUP_NAME" zookeeper
echo "id zookeeper"
id zookeeper
echo "groups zookeeper"
groups zookeeper
pwd
cd /tmp
echo "sudo rm -r /opt/z1   CLEANNING"
echo "sudo rm -r /opt/z2   CLEANNING"
echo "sudo rm -r /opt/z3   CLEANNING"
sudo rm -r /opt/z1
sudo rm -r /opt/z2
sudo rm -r /opt/z3
wget https://downloads.apache.org/zookeeper/zookeeper-${ZOOKEEPER_VER}/apache-zookeeper-${ZOOKEEPER_VER}-bin.tar.gz
sudo mkdir /opt/z1
sudo mkdir /opt/z2
sudo mkdir /opt/z3
sudo tar -xvzf apache-zookeeper-${ZOOKEEPER_VER}-bin.tar.gz -C /opt/z1/
## Location: /opt/z1/bin/ and /opt/z2/bin/   (or /opt/z1/bin/ and /usr/share/zookeeper2/bin/)
sudo tar -xvzf apache-zookeeper-${ZOOKEEPER_VER}-bin.tar.gz -C /opt/z2/
sudo tar -xvzf apache-zookeeper-${ZOOKEEPER_VER}-bin.tar.gz -C /opt/z3/

##sudo cp -pr apache-zookeeper-${ZOOKEEPER_VER}-bin/*.* /opt/z1/apache-zookeeper-${ZOOKEEPER_VER}-bin
sudo cp -pr /home/${USER_NAME_IN_OS}/KD/_KDPROJ/tmp/zoo1/zoo.cfg /opt/z1/apache-zookeeper-${ZOOKEEPER_VER}-bin/conf
sudo cp -pr /home/${USER_NAME_IN_OS}/KD/_KDPROJ/tmp/zoo1/java.env /opt/z1/apache-zookeeper-${ZOOKEEPER_VER}-bin/conf
sudo cp -pr /home/${USER_NAME_IN_OS}/KD/_KDPROJ/tmp/zoo2/zoo.cfg /opt/z2/apache-zookeeper-${ZOOKEEPER_VER}-bin/conf
sudo cp -pr /home/${USER_NAME_IN_OS}/KD/_KDPROJ/tmp/zoo2/java.env /opt/z2/apache-zookeeper-${ZOOKEEPER_VER}-bin/conf
sudo cp -pr /home/${USER_NAME_IN_OS}/KD/_KDPROJ/tmp/zoo3/zoo.cfg /opt/z3/apache-zookeeper-${ZOOKEEPER_VER}-bin/conf
sudo cp -pr /home/${USER_NAME_IN_OS}/KD/_KDPROJ/tmp/zoo3/java.env /opt/z3/apache-zookeeper-${ZOOKEEPER_VER}-bin/conf

pwd
echo "&&&&&&&&&&&&&&&&&&&&01"
mkdir /opt/z1/apache-zookeeper-${ZOOKEEPER_VER}-bin/data
mkdir /opt/z1/apache-zookeeper-${ZOOKEEPER_VER}-bin/logs
echo "1" > /opt/z1/apache-zookeeper-${ZOOKEEPER_VER}-bin/data/myid 
mkdir /opt/z2/apache-zookeeper-${ZOOKEEPER_VER}-bin/data
mkdir /opt/z2/apache-zookeeper-${ZOOKEEPER_VER}-bin/logs
echo "2" > /opt/z2/apache-zookeeper-${ZOOKEEPER_VER}-bin/data/myid 
mkdir /opt/z3/apache-zookeeper-${ZOOKEEPER_VER}-bin/data
mkdir /opt/z3/apache-zookeeper-${ZOOKEEPER_VER}-bin/logs
echo "3" > /opt/z3/apache-zookeeper-${ZOOKEEPER_VER}-bin/data/myid 
sudo chown -R zookeeper:${MISE_GROUP_NAME} /opt/z1
sudo chown -R zookeeper:${MISE_GROUP_NAME} /opt/z2
sudo chown -R zookeeper:${MISE_GROUP_NAME} /opt/z3
sudo chown -R zookeeper:${MISE_GROUP_NAME} /opt/z1/apache-zookeeper-${ZOOKEEPER_VER}-bin
sudo chown -R zookeeper:${MISE_GROUP_NAME} /opt/z2/apache-zookeeper-${ZOOKEEPER_VER}-bin
sudo chown -R zookeeper:${MISE_GROUP_NAME} /opt/z3/apache-zookeeper-${ZOOKEEPER_VER}-bin
sudo chown -R zookeeper:${MISE_GROUP_NAME} /opt/z1/apache-zookeeper-${ZOOKEEPER_VER}-bin/bin
sudo chown -R zookeeper:${MISE_GROUP_NAME} /opt/z2/apache-zookeeper-${ZOOKEEPER_VER}-bin/bin
sudo chown -R zookeeper:${MISE_GROUP_NAME} /opt/z3/apache-zookeeper-${ZOOKEEPER_VER}-bin/bin
echo "source ls -al /opt/z1/"
source ls -al /opt/z1/
echo "source ls -al /opt/z2/"
source ls -al /opt/z2/
echo "source ls -al /opt/z3/"
source ls -al /opt/z3/
echo "source ls -al /opt/z1/apache-zookeeper-${ZOOKEEPER_VER}-bin/"
source ls -al /opt/z1/apache-zookeeper-${ZOOKEEPER_VER}-bin/
echo "source ls -al /opt/z2/apache-zookeeper-${ZOOKEEPER_VER}-bin/"
source ls -al /opt/z2/apache-zookeeper-${ZOOKEEPER_VER}-bin/
echo "source ls -al /opt/z3/apache-zookeeper-${ZOOKEEPER_VER}-bin/"
source ls -al /opt/z3/apache-zookeeper-${ZOOKEEPER_VER}-bin/
echo "source ls -al /opt/z1/apache-zookeeper-${ZOOKEEPER_VER}-bin/bin/"
source ls -al /opt/z1/apache-zookeeper-${ZOOKEEPER_VER}-bin/bin/
echo "source ls -al /opt/z2/apache-zookeeper-${ZOOKEEPER_VER}-bin/bin/"
source ls -al /opt/z2/apache-zookeeper-${ZOOKEEPER_VER}-bin/bin/
echo "source ls -al /opt/z3/apache-zookeeper-${ZOOKEEPER_VER}-bin/bin/"
source ls -al /opt/z3/apache-zookeeper-${ZOOKEEPER_VER}-bin/bin/

export "PATH=$PATH:/opt/z1/apache-zookeeper-${ZOOKEEPER_VER}-bin/bin:/opt/z2/apache-zookeeper-${ZOOKEEPER_VER}-bin/bin:/opt/z3/apache-zookeeper-${ZOOKEEPER_VER}-bin/bin"
sudo echo "PATH=$PATH:/opt/z1/apache-zookeeper-${ZOOKEEPER_VER}-bin/bin:/opt/z2/apache-zookeeper-${ZOOKEEPER_VER}-bin/bin:/opt/z3/apache-zookeeper-${ZOOKEEPER_VER}-bin/bin" >> ~/.bashrc
source ~/.bashrc

cd /opt/z1/apache-zookeeper-${ZOOKEEPER_VER}-bin/
## 97_01_start_services_before_build.sh: ### java -cp ./lib/zookeeper-${ZOOKEEPER_VER}.jar:lib/*:conf org.apache.zookeeper.server.quorum.QuorumPeerMain ./conf/zoo.cfg >> logs/zookeeper.log & 
# java -cp zookeeper.jar:lib/*:conf org.apache.zookeeper.server.quorum.QuorumPeerMain zoo.cfg
cd /opt/z1/apache-zookeeper-${ZOOKEEPER_VER}-bin/
## 97_01_start_services_before_build.sh: ### source ./bin/zkCli.sh -server 127.0.0.1:${ZOOKEEPER_PORT_1} 
## 97_01_start_services_before_build.sh: ### source ./bin/zkCli.sh -server 127.0.0.1:${ZOOKEEPER_PORT_2} 
## 97_01_start_services_before_build.sh: ### source ./bin/zkCli.sh -server 127.0.0.1:${ZOOKEEPER_PORT_3} 
## This can be run as a cron job on the ZooKeeper server machines to clean up the logs daily.
# java -cp zookeeper.jar:lib/slf4j-api-1.7.5.jar:lib/slf4j-log4j12-1.7.5.jar:lib/log4j-1.2.17.jar:conf org.apache.zookeeper.server.PurgeTxnLog <dataDir> <snapDir> -n <count>

pwd
echo "&&&&&&&&&&&&&&&&&&&&02"
## https://medium.com/@ryannel/installing-zookeeper-on-ubuntu-9f1f70f22e25
## Debug Zookeeper:
## The best way to debug Zookeeper is to run it in the foreground so you can watch the output directly.
## 97_01_start_services_before_build.sh: ### source /opt/z1/apache-zookeeper-${ZOOKEEPER_VER}-bin/bin/zkServer.sh start-foreground
## 97_01_start_services_before_build.sh: ### source /opt/z1/apache-zookeeper-${ZOOKEEPER_VER}-bin/bin/zkServer.sh start-foreground
## Start Zookeeper:
#sudo chmod 0775 /home/${USER_NAME_IN_OS}/KD/_KDPROJ/tmp/LOGS
#touch zookeeper1_active.log
## 97_01_start_services_before_build.sh: ### sudo mv zookeeper1_active.log /home/${USER_NAME_IN_OS}/KD/_KDPROJ/tmp/LOGS
#touch zookeeper2_active.log
## 97_01_start_services_before_build.sh: ### sudo mv zookeeper2_active.log /home/${USER_NAME_IN_OS}/KD/_KDPROJ/tmp/LOGS
#touch zookeeper3_active.log
## 97_01_start_services_before_build.sh: ### sudo mv zookeeper3_active.log /home/${USER_NAME_IN_OS}/KD/_KDPROJ/tmp/LOGS
## 97_01_start_services_before_build.sh: ### source /opt/z1/apache-zookeeper-${ZOOKEEPER_VER}-bin/bin/zkServer.sh start > "/home/${USER_NAME_IN_OS}/KD/_KDPROJ/tmp/LOGS/zookeeper1_active.log"
## 97_01_start_services_before_build.sh: ### source /opt/z2/apache-zookeeper-${ZOOKEEPER_VER}-bin/bin/zkServer.sh start > "/home/${USER_NAME_IN_OS}/KD/_KDPROJ/tmp/LOGS/zookeeper2_active.log"
## 97_01_start_services_before_build.sh: ### source /opt/z3/apache-zookeeper-${ZOOKEEPER_VER}-bin/bin/zkServer.sh start > "/home/${USER_NAME_IN_OS}/KD/_KDPROJ/tmp/LOGS/zookeeper3_active.log"

## Check the status?? of Zookeeper:
## 97_01_start_services_before_build.sh: ### source /opt/z1/apache-zookeeper-${ZOOKEEPER_VER}-bin/bin/zkServer.sh status
## 97_01_start_services_before_build.sh: ### source /opt/z2/apache-zookeeper-${ZOOKEEPER_VER}-bin/bin/zkServer.sh status
## 97_01_start_services_before_build.sh: ### source /opt/z3/apache-zookeeper-${ZOOKEEPER_VER}-bin/bin/zkServer.sh status
pwd
echo "tail -f /home/${USER_NAME_IN_OS}/KD/_KDPROJ/tmp/LOGS/zookeeper1_active.log" >> $PROJECT_HOME/_ubuntu_DO_ALL/08_02_sudo_kubeadm_join.sh
echo "tail -f /home/${USER_NAME_IN_OS}/KD/_KDPROJ/tmp/LOGS/zookeeper2_active.log" >> $PROJECT_HOME/_ubuntu_DO_ALL/08_02_sudo_kubeadm_join.sh
echo "tail -f /home/${USER_NAME_IN_OS}/KD/_KDPROJ/tmp/LOGS/zookeeper3_active.log" >> $PROJECT_HOME/_ubuntu_DO_ALL/08_02_sudo_kubeadm_join.sh


echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
##OR#

## Installing Apache Kafka... CHECK THE LAST VERSION AT https://downloads.apache.org/kafka/

## https://devops.ionos.com/tutorials/install-and-configure-apache-kafka-on-ubuntu-1604-1/
## https://www.apache.org/dyn/closer.cgi?path=/kafka/2.5.0/kafka_2.12-2.5.0.tgz
## https://my-bigdata-blog.blogspot.com/search/label/Kafka

cd /tmp
echo "sudo rm -r /opt/kafka/   CLEANNING"
sudo rm -r /opt/kafka/
# export KAFKA_PORT=9092
wget https://downloads.apache.org/kafka/${KAFKA_VER_2}/kafka_${KAFKA_VER_1}-${KAFKA_VER_2}.tgz
sudo mkdir /opt/kafka
sudo tar -xvf kafka_${KAFKA_VER_1}-${KAFKA_VER_2}.tgz -C /opt/kafka/
cd /opt/kafka/kafka_${KAFKA_VER_1}-${KAFKA_VER_2}/
touch zookeeper1_active.log
sudo mv zookeeper1_active.log /opt/z1/apache-zookeeper-${ZOOKEEPER_VER}-bin/logs/
## The next step is to start Kafka server, you can start it by running kafka-server-start.sh script 
## located at /opt/kafka/kafka_${KAFKA_VER_1}-${KAFKA_VER_2}/bin/ directory.
sudo  /opt/kafka/kafka_${KAFKA_VER_1}-${KAFKA_VER_2}/bin/kafka-server-start.sh /opt/kafka/kafka_${KAFKA_VER_1}-${KAFKA_VER_2}/config/server.properties

cd /opt/kafka/kafka_${KAFKA_VER_1}-${KAFKA_VER_2}/logs/
ls -al

## You should see the following output, if the server has started successfully:
## [2019-08-22 21:43:48,279] WARN No meta.properties file under dir /tmp/kafka-logs/meta.properties (kafka.server.BrokerMetadataCheckpoint)
## [2019-08-22 21:43:48,516] INFO Kafka version : 0.10.0.1 (org.apache.kafka.common.utils.AppInfoParser)
## [2019-08-22 21:43:48,525] INFO Kafka commitId : a7a17cdec9eaa6c5 (org.apache.kafka.common.utils.AppInfoParser)
## [2019-08-22 21:43:48,527] INFO [Kafka Server 0], started (kafka.server.KafkaServer)
## [2019-08-22 21:43:48,555] INFO New leader is 0 (kafka.server.ZookeeperLeaderElector$LeaderChangeListener)

echo "&&&&&&&&&&&&&&&&&&&&01"
## You can use nohup with script to start the Kafka server as a background process:
#sudo nohup /opt/kafka/kafka_${KAFKA_VER_1}-${KAFKA_VER_2}/bin/kafka-server-start.sh /opt/kafka/kafka_${KAFKA_VER_1}-${KAFKA_VER_2}/config/server.properties /tmp/kafka.log &

## You now have a Kafka server running and listening on port ${KAFKA_PORT}.
## Testing Kafka Server
## Now, it is time to verify the Kafka server is operating correctly.
## To test Kafka, create a sample topic with name "testing" in Apache Kafka using the following command:


gnome-terminal &
sudo /opt/kafka/kafka_${KAFKA_VER_1}-${KAFKA_VER_2}/bin/kafka-topics.sh --create --zookeeper localhost:${ZOOKEEPER_PORT_1} --replication-factor 1  --partitions 1 --topic testing

## You should see the following output:
## Created topic "testing".
## Now, ask Zookeeper to list available topics on Apache Kafka by running the following command:

sudo /opt/kafka/kafka_${KAFKA_VER_1}-${KAFKA_VER_2}/bin/kafka-topics.sh --list --zookeeper localhost:${ZOOKEEPER_PORT_1}

echo "&&&&&&&&&&&&&&&&&&&&02"
## You should see the following output:
#  testing
## Now, publish a sample messages to Apache Kafka topic called testing by using the following producer command:

sudo /opt/kafka/kafka_${KAFKA_VER_1}-${KAFKA_VER_2}/bin/kafka-console-producer.sh --broker-list localhost:${KAFKA_PORT} --topic testing

## After running above command, enter some messages like "Hi how are you?" press enter, then enter another message like "Where are you?"
# http://kafka.apache.org/quickstart
# >
"Hi how are you?"
"Where are you?"

echo "&&&&&&&&&&&&&&&&&&&&03"
## Now, use consumer command to check for messages on Apache Kafka Topic called testing by running the following command:

gnome-terminal &
sudo /opt/kafka/kafka_${KAFKA_VER_1}-${KAFKA_VER_2}/bin/kafka-console-consumer.sh --zookeeper localhost:${ZOOKEEPER_PORT_1} --topic testing --from-beginning
##?? sudo /opt/kafka/kafka_${KAFKA_VER_1}-${KAFKA_VER_2}/bin/kafka-console-consumer.sh --zookeeper localhost:${ZOOKEEPER_PORT_1} --topic testing  --bootstrap-server localhost

## You should see the following output:
#  Hi how are you?
#  Where are you?
## With this above testing you have successfully verified that you have a valid Apache Kafka setup with Apache Zookeeper.


echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
##OR#

## Installing Netflix EUREKA...
##  https://github.com/Netflix/eureka/wiki/Building-Eureka-Client-and-Server
##  https://spring.io/blog/2015/01/20/microservice-registration-and-discovery-with-spring-cloud-and-netflix-s-eureka
##  http://www.dowdandassociates.com/blog/content/howto-setup-netflix-eureka-v1-x/
pwd
cd $PROJECT_HOME
pwd
cd ..
pwd
echo "sudo rm -r EUREKA_build    CLEANNING"
sudo rm -r EUREKA_build
mkdir EUREKA_build
echo "chown -RH $USER_NAME_IN_OS: EUREKA_build"
sudo chown -RH $USER_NAME_IN_OS: EUREKA_build
echo "sudo chgrp -R $MISE_GROUP_NAME EUREKA_build"
sudo chgrp -R $MISE_GROUP_NAME EUREKA_build
cd EUREKA_build
sudo ls -al
## https://search.maven.org/search?q=eureka-server
#eureka-server-1.9.22.war
#you can merge in the edited property files under WEB-INF/classes yourself
##https://github.com/Netflix/eureka/wiki/Running-the-Demo-Application
#cp ./eureka-server/build/libs/eureka-server-XXX-SNAPSHOT.war $TOMCAT_HOME/webapps/eureka.war
#Start your tomcat server. Access http://localhost:8080/eureka 
#https://github.com/Netflix/eureka/tree/master/eureka-examples
#https://github.com/Netflix/eureka/blob/master/eureka-server/src/main/resources/eureka-server.properties
## Create (or add to) a setenv.sh in tomcat/bin/ with the following java opts (these are for the demo server to start up fast, 
## see EurekaServerConfig.java for their documentation):
## https://github.com/Netflix/eureka/blob/master/eureka-core/src/main/java/com/netflix/eureka/EurekaServerConfig.java
#
#JAVA_OPTS=" \
#  -Deureka.waitTimeInMsWhenSyncEmpty=0 \
#  -Deureka.numberRegistrySyncRetries=0"
sudo cp $PROJECT_HOME/conf/eureka/eureka$EUREKA_SERVER_VER.war .
sudo chown -RH $USER_NAME_IN_OS: ./eureka$EUREKA_SERVER_VER.war
sudo ls -al

sudo cp ./eureka$EUREKA_SERVER_VER.war  /opt/tomcat/apache-tomcat-$TOMCAT_VER/webapps/eureka$EUREKA_SERVER_VER.war
sudo chown -RH $USER_NAME_IN_OS:  /opt/tomcat/apache-tomcat-$TOMCAT_VER/webapps/eureka$EUREKA_SERVER_VER.war

echo "sudo git clone https://github.com/Netflix/eureka.git !!!!!!!!!!"
sudo git clone https://github.com/Netflix/eureka.git

## https://howtodoinjava.com/spring-cloud/spring-cloud-service-discovery-netflix-eureka/
## https://www.baeldung.com/spring-cloud-netflix-eureka
## https://itnext.io/how-to-use-netflixs-eureka-and-spring-cloud-for-service-registry-8b43c8acdf4e
## https://spring.io/guides/gs/service-registration-and-discovery/
## https://dzone.com/articles/standing-local-netflix-eureka
## https://dev.to/nagarajendra/netflix-eureka-server-and-client-setup-with-spring-boot-3j9n
## https://spring.io/guides/gs/routing-and-filtering/   ## Zuul

## http://www.gattis.org/Home/kibanaroundingto16significantdigits

#################
##  org.springframework.boot:spring-cloud-starter-eureka-server on your classpath.
##  import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer; in app
##  @EnableEurekaServer
#https://www.baeldung.com/spring-cloud-netflix-eureka
## Create a Spring boot project from initializer portal with four dependencies:
##  i.e. Actuator, Web, Rest Repositories, Eureka Discovery.
##  import org.springframework.cloud.netflix.eureka.EnableEurekaClient; in app
##  import org.springframework.cloud.netflix.feign.EnableFeignClients;
##  import org.springframework.cloud.netflix.feign.FeignClient;
##  @EnableEurekaClient
##  @EnableFeignClients
#https://spring.io/blog/2015/01/20/microservice-registration-and-discovery-with-spring-cloud-and-netflix-s-eureka
#import org.apache.commons.lang.builder.ToStringBuilder;
#import org.springframework.beans.factory.annotation.Autowired;
#import org.springframework.boot.CommandLineRunner;
#import org.springframework.boot.autoconfigure.SpringBootApplication;
#import org.springframework.boot.builder.SpringApplicationBuilder;
#import org.springframework.cloud.client.ServiceInstance;
#import org.springframework.cloud.client.discovery.DiscoveryClient;
#import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
#import org.springframework.cloud.netflix.feign.EnableFeignClients;
#import org.springframework.cloud.netflix.feign.FeignClient;
#import org.springframework.core.ParameterizedTypeReference;
#import org.springframework.http.HttpMethod;
#import org.springframework.http.ResponseEntity;
#import org.springframework.stereotype.Component;
#import org.springframework.web.bind.annotation.PathVariable;
#import org.springframework.web.bind.annotation.RequestMapping;
#import org.springframework.web.bind.annotation.RequestMethod;
#import org.springframework.web.client.RestTemplate;

## https://howtodoinjava.com/spring-cloud/spring-cloud-service-discovery-netflix-eureka/

## GRADLE:
#https://itnext.io/how-to-use-netflixs-eureka-and-spring-cloud-for-service-registry-8b43c8acdf4e
#https://spring.io/guides/gs/service-registration-and-discovery/

echo "&&&&&&&&&&&&&&&&&&&&01"


##  https://github.com/Netflix/eureka/wiki/Configuring-Eureka
##  https://github.com/Netflix/eureka/wiki/Building-Eureka-Client-and-Server
## https://search.maven.org/search?q=eureka-server
#eureka-server-1.9.22.war
#you can merge in the edited property files under WEB-INF/classes yourself.


## Build the Eureka Server by executing the following in the directory where you pulled your sources:
pwd
cd $PROJECT_HOME
pwd
cd ..
cd EUREKA_build
touch eureka_active.log
sudo chmod 0775 /home/${USER_NAME_IN_OS}/KD/_KDPROJ/tmp/LOGS
ls -al /home/${USER_NAME_IN_OS}/KD/_KDPROJ/tmp/LOGS
sudo mv eureka_active.log /home/${USER_NAME_IN_OS}/KD/_KDPROJ/tmp/LOGS/
cd eureka
sudo cp $PROJECT_HOME/conf/eureka/pom.xml ./eureka
## sudo ./gradlew clean build > '/home/${USER_NAME_IN_OS}/KD/_KDPROJ/tmp/LOGS/eureka_active.log'
sudo /opt/gradle/bin/gradle tasks

sudo $MAVEN_RUN -X -T 4 -Dmaven.test.skip=true clean package spring-boot:repackage
## [ERROR] Failed to execute goal org.springframework.boot:spring-boot-maven-plugin:2.0.0.RELEASE:repackage (default) 
## on project eureka-service: Execution default of goal org.springframework.boot:spring-boot-maven-plugin:2.0.0.RELEASE:repackage failed: 
## Unable to find main class
sudo $MAVEN_RUN -X -T 4 -Dmaven.test.skip=true clean package spring-boot:repackage && java -jar target/eureka-service-0.1.0.jar


## sudo $MAVEN_RUN -X spring-boot:run
#sudo /opt/gradle/bin/gradle --stacktrace clean build 

## https://hub.docker.com/r/springcloud/eureka/
## https://hub.docker.com/r/netflixoss/eureka/dockerfile
## https://medium.com/@sasisugumar/containerize-spring-cloud-eureka-server-c3af66578026
## https://stackoverflow.com/questions/49453397/run-eureka-service-in-a-docker-container
## https://exampledriven.wordpress.com/2016/06/24/spring-boot-docker-example/



## HOW TO run eureka in docker:
#mvn package && java -jar target/eureka-service-0.1.0.jar

## running mvn package && java -jar target/eureka-service-0.1.0.jar to build the jar. 
## During this the spring boot application and I have to stop this using ctrl+c. 
## The image has been created in the target folder.
## building image using mvn install dockerfile:build. Image has been built.
#mvn install dockerfile:build

## run the created image using docker run -p 8080:8080 -t microservice/eureka-service
#docker run -p 8080:8080 -t microservice/eureka-service

## This is the respone
## 2018-03-23 15:28:05.618  INFO 1 --- [           main] hello.EurekaServiceApplication           : Started EurekaServiceApplication in 17.873 seconds (JVM running for 19.066) 2018-03-23 15:29:05.475  INFO 1 --- [a-EvictionTimer] c.n.e.registry.AbstractInstanceRegistry  : Running the evict task with compensationTime 0ms


gnome-terminal &
echo 'tail -f /home/${USER_NAME_IN_OS}/KD/_KDPROJ/tmp/LOGS/eureka_active.log' >> ./08_02_sudo_kubeadm_join.sh

############ Ctrl+C   !!!! to continue....

## You can find the following artifacts:
# 1.   Eureka Server WAR archive (./eureka-server/build/libs/eureka-server-XXX.war )
# 2.   Eureka Client (./eureka-client/build/libs/eureka-client-XXX.jar )
# 3.   Dependencies (./eureka-server/testlibs/) (If you do not want to use maven to download dependencies you can use these archives)

## Server Configuration ############################################

## --------------------- src/main/resources/application.yml -----------------------------
## Create one file called application.yml in the src/main/resources/ directory. Add these properties:
#server:
#  port: 8761 # Indicate the default PORT where this service will be started
# 
#eureka:
#  client:
#    registerWithEureka: false   #telling the server not to register himself in the service registry
#    fetchRegistry: false
#  server:
#    waitTimeInMsWhenSyncEmpty: 0    #wait time for subsequent sync

## --------------------- src/main/resources/bootstrap.yml -----------------------------
## Create another file called bootstrap.yml in the src/main/resources/ directory. Add these properties:
#spring:
#  application:
#    name: eureka
#  cloud:
#    config:
#      uri: ${CONFIG_SERVER_URL=http://localhost:8888}


## Client Configuration ############################################

## --------------------- src/main/resources/application.yml -----------------------------
## Create one file called application.yml in the src/main/resources directory and add below lines:
#server:
#  port: 8098    #default port where the service will be started
# 
#eureka:         #tells about the Eureka server details and its refresh time
#  instance:
#    leaseRenewalIntervalInSeconds: 1
#    leaseExpirationDurationInSeconds: 2
#  client:
#    serviceUrl:
#      defaultZone: http://127.0.0.1:8761/eureka/
#    healthcheck:
#      enabled: true
#    lease:
#      duration: 5
# 
#spring:     
#  application:
#    name: student-service   #current service name to be used by the eureka server
#     
#management:
#  security:
#    enabled: false  #disable the spring security on the management endpoints like /env, /refresh etc. 
# 
#logging:
#  level:
#    pl.com.microservices: DEBUG


## Add REST API ############################################

## Now add one RestController and expose one rest endpoint for .............


echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo "********************************************************************************"
