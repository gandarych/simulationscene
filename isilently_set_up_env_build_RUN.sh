#!/bin/bash


#set -o allexport
#[[ -f ./_ubuntu_DO_ALL/env.txt ]] && source ./_ubuntu_DO_ALL/env.txt
#set +o allexport


set -a
[ -f ./_ubuntu_DO_ALL/env.txt ] && . ./_ubuntu_DO_ALL/env.txt
set +a



source ./_ubuntu_DO_ALL/env.txt
export $(cut -d= -f1 ./_ubuntu_DO_ALL/env.txt)
## source ./_ubuntu_DO_ALL/00_01_EnvVAR_check_new_versions.sh

source ./_ubuntu_DO_ALL/01_02_update_ubuntu.sh
source ./_ubuntu_DO_ALL/01_04_install_openjdk.sh
source ./_ubuntu_DO_ALL/01_05_install_htop_gparted_mc.sh
source ./_ubuntu_DO_ALL/01_06_add_screen_recorder.sh
source ./_ubuntu_DO_ALL/02_01_install_maven_gradle.sh
bash ./_ubuntu_DO_ALL/02_02_install_3_IDEs.sh
source ./_ubuntu_DO_ALL/02_03_install_tomcat.sh
source ./_ubuntu_DO_ALL/02_04_add_tomcat_user.sh
source ./_ubuntu_DO_ALL/02_05_install_discovery_kafka.sh
source ./_ubuntu_DO_ALL/02_06_install_secrets.sh
source ./_ubuntu_DO_ALL/03_01_vi_tomcat_service.sh
source ./_ubuntu_DO_ALL/03_02_systemctl_start_tomcat.sh
source ./_ubuntu_DO_ALL/03_03_install_packer.sh
source ./_ubuntu_DO_ALL/03_04_ruby_sinatra.sh
source ./_ubuntu_DO_ALL/04_01_install_docker.sh

# source ./_ubuntu_DO_ALL/04_02_using_docker_cmd_AS_ROOT.sh
source ./_ubuntu_DO_ALL/04_03_docker-compose_up.sh
# source ./_ubuntu_DO_ALL/05_01_install_ansible_AS_ROOT.sh
# source ./_ubuntu_DO_ALL/06_01_install_terraform_AS_ROOT.sh
# source ./_ubuntu_DO_ALL/07_01_install_packer_AS_ROOT.sh

source ./_ubuntu_DO_ALL/07_01_install_Golang.sh
source ./_ubuntu_DO_ALL/08_01_install_kubernetes.sh

# source ./_ubuntu_DO_ALL/08_02_sudo_kubeadm_JOIN.sh
echo "# source ./_ubuntu_DO_ALL/bash 08_02_sudo_kubeadm_JOIN.sh"

# source jsilently_MVN_build_all_deploy_and_run_EN.sh
