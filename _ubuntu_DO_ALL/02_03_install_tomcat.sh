#!/bin/bash

## 02_03_install_tomcat on ubuntu

echo "pwd =" $(pwd)
echo "USER_NAME_IN_OS = $USER_NAME_IN_OS, hostname =" $(hostname)
echo "date =" $(date)
echo "HOME = $HOME, USER = $USER, LOGNAME = $LOGNAME"
echo "By value: KAFKA_PORT = " $KAFKA_PORT ${KAFKA_PORT} "$KAFKA_PORT ${KAFKA_PORT}"
echo "By value: TOMCAT_VER = " $TOMCAT_VER ${TOMCAT_VER} "$TOMCAT_VER ${TOMCAT_VER}"

if [[ ! -z "$(cut -d= -f1 /home/$USER_NAME_IN_OS/.bashrc | grep declare)" ]]
then
	echo 'Environment variables are set.'
		echo "$(cut -d= -f1 /home/$USER_NAME_IN_OS/.bashrc | grep declare)"
fi

echo "%%%%%%%%%%%%%%%%%%"

pwd
cd $PROJECT_HOME
pwd
echo "RUNNING 02_03_install_tomcat.sh"

## remove old if exists:
echo "sudo rm -r /opt/tomcat/   CLEANING"
sudo rm -r /opt/tomcat/
echo "sudo mkdir /opt/tomcat"
sudo mkdir /opt/tomcat
## https://medium.com/@madeeshafernando/installing-apache-tomcat-on-ubuntu-18-04-8cf8bc63993d
echo "cd /tmp"
cd /tmp
echo "sudo wget https://archive.apache.org/dist/tomcat/tomcat-9/v${TOMCAT_VER}/bin/apache-tomcat-${TOMCAT_VER}.tar.gz"
sudo wget https://archive.apache.org/dist/tomcat/tomcat-9/v$TOMCAT_VER/bin/apache-tomcat-$TOMCAT_VER.tar.gz
## extract downloaded the Tomcat archive and move it to the /opt/tomcat directory.
echo "sudo tar xf /tmp/apache-tomcat-9*.tar.gz -C /opt/tomcat"
sudo tar xf /tmp/apache-tomcat-9*.tar.gz -C /opt/tomcat
echo "sudo rm /tmp/apache-tomcat-9*.tar.gz"
sudo rm /tmp/apache-tomcat-9*.tar.gz
echo "sudo mkdir /opt/tomcat/apache-tomcat-KEEPOUT"
sudo mkdir /opt/tomcat/apache-tomcat-KEEPOUT
echo "sudo cp -i -r /opt/tomcat/apache-tomcat-$TOMCAT_VER/* /opt/tomcat/apache-tomcat-KEEPOUT"
sudo cp -i -r /opt/tomcat/apache-tomcat-$TOMCAT_VER/* /opt/tomcat/apache-tomcat-KEEPOUT
echo "cd /opt/tomcat/apache-tomcat-$TOMCAT_VER"
cd /opt/tomcat/apache-tomcat-$TOMCAT_VER

echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo "********************************************************************************"
