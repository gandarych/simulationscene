#!/bin/bash

###### 07_01_install_hcl.sh on ubuntu

echo "pwd =" $(pwd)
echo "USER_NAME_IN_OS = $USER_NAME_IN_OS, hostname =" $(hostname)
echo "date =" $(date)
echo "HOME = $HOME, USER = $USER, LOGNAME = $LOGNAME"
echo "By value: KAFKA_PORT = " $KAFKA_PORT ${KAFKA_PORT} "$KAFKA_PORT ${KAFKA_PORT}"
echo "By value: TOMCAT_VER = " $TOMCAT_VER ${TOMCAT_VER} "$TOMCAT_VER ${TOMCAT_VER}"

if [[ ! -z "$(cut -d= -f1 /home/$USER_NAME_IN_OS/.bashrc | grep declare)" ]]
then
	echo 'Environment variables are set.'
		echo "$(cut -d= -f1 /home/$USER_NAME_IN_OS/.bashrc | grep declare)"
fi

echo "%%%%%%%%%%%%%%%%%%"

pwd
cd $PROJECT_HOME
pwd
echo "RUNNING 07_01_install_hcl.sh"

echo "terraform"
terraform

echo "&&&&&&&&&&&&&&&&&&&&&&&&&&&&&1"
echo "terraform --help"
terraform --help

echo "&&&&&&&&&&&&&&&&&&&&&&&&&&&&&2"
echo "terraform graph -h"
terraform graph -h

#  https://learn.hashicorp.com/terraform/getting-started/install


mkdir terraform-docker-demo && cd $_

sudo touch main.tf

sudo echo 'resource "docker_image" "nginx" {' > main.tf
sudo echo '  name         = "nginx:latest"' >> main.tf
sudo echo '  keep_locally = false' >> main.tf
sudo echo '}' >> main.tf
sudo echo 'resource "docker_container" "nginx" {' >> main.tf
sudo echo '  image = docker_image.nginx.latest' >> main.tf
sudo echo '  name  = "tutorial"' >> main.tf
sudo echo '  ports {' >> main.tf
sudo echo '    internal = 80' >> main.tf
sudo echo '    external = 8000' >> main.tf
sudo echo '  }' >> main.tf
sudo echo '}' >> main.tf

echo "&&&&&&&&&&&&&&&&&&&&&&&&&&&&&3"

## Initialize the project, which downloads a plugin that allows Terraform to interact with Docker.
terraform init

echo "&&&&&&&&&&&&&&&&&&&&&&&&&&&&&4"
## Provision the NGINX server container with apply. When Terraform asks you to confirm type yes and press ENTER.
terraform apply

echo "&&&&&&&&&&&&&&&&&&&&&&&&&&&&&5"
## Verify the existence of the NGINX container by visiting localhost:8000 in your web browser or running docker ps to see the container.
## NGINX running in Docker via Terraform
docker ps


#  https://www.terraform.io/docs/commands/index.html

#  https://github.com/hashicorp/hcl2

#  https://github.com/hashicorp/hcl

#  https://www.linode.com/docs/applications/configuration-management/introduction-to-hcl/

#  https://www.linode.com/docs/applications/configuration-management/introduction-to-hcl/

####   https://github.com/startup-systems/terraform-ansible-example
####   https://www.hashicorp.com/resources/ansible-terraform-better-together
####   https://alex.dzyoba.com/blog/terraform-ansible/
####   https://medium.com/faun/building-repeatable-infrastructure-with-terraform-and-ansible-on-aws-3f082cd398ad
####   https://victorops.com/blog/writing-ansible-playbooks-for-new-terraform-servers
####   https://getintodevops.com/blog/using-ansible-with-terraform
####   https://www.redhat.com/cms/managed-files/pa-terraform-and-ansible-overview-f14774wg-201811-en.pdf
####   https://stackshare.io/stackups/ansible-vs-terraform    (LOGO, following)

####   Packer provisioner runs Ansible playbooks!!!:
####   https://packer.io/docs/provisioners/ansible.html

echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo "********************************************************************************"
