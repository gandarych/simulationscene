#!/bin/bash

## 02_08_install_hadoop.sh on ubuntu

echo "pwd =" $(pwd)
echo "USER_NAME_IN_OS = $USER_NAME_IN_OS, hostname =" $(hostname)
echo "date =" $(date)
echo "HOME = $HOME, USER = $USER, LOGNAME = $LOGNAME"
echo "By value: KAFKA_PORT = " $KAFKA_PORT ${KAFKA_PORT} "$KAFKA_PORT ${KAFKA_PORT}"
echo "By value: TOMCAT_VER = " $TOMCAT_VER ${TOMCAT_VER} "$TOMCAT_VER ${TOMCAT_VER}"

if [[ ! -z "$(cut -d= -f1 /home/$USER_NAME_IN_OS/.bashrc | grep declare)" ]]
then
	echo 'Environment variables are set.'
		echo "$(cut -d= -f1 /home/$USER_NAME_IN_OS/.bashrc | grep declare)"
fi

echo "%%%%%%%%%%%%%%%%%%"

## https://itwiz.pl/hadoop-czyli-przetwarzanie-rozproszone-open-source/

pwd
cd $PROJECT_HOME
pwd

## add-apt-repository -y = silent execution of this command
## >> /tmp/output.txt
## The will tell Bash to redirect stderr to stdout.
# sudo add-apt-repository -y ppa:fossproject/ppa >> /tmp/output.txt
## Or use output redirection to /dev/null and include stderr output.
# sudo add-apt-repository -y ppa:fossproject/ppa >> /dev/null


echo "RUNNING 02_08_install_hadoop.sh.sh"

## Installing Apache Hadoop
## https://tecadmin.net/setup-hadoop-on-ubuntu/

echo "sudo useradd -r -m -U -d /opt/hadoop -s /bin/false hadoop"
## Creation a non-login user 'hadoop':
sudo useradd -r -m -U -d /opt/hadoop -s /bin/false hadoop
##  -r, --system                        create a system account
##  -m, --create-home               create the user's home directory
##  -U, --user-group                  create a group with the same name as the user
##  -d, --home-dir HOME_DIR   home directory of the new account
##  -s, --shell SHELL                login shell of the new account
##  -g, --gid GROUP                 name or ID of the primary group of the new account, if not the same name as the user

sudo usermod -a -G "$MISE_GROUP_NAME" hadoop

echo "id hadoop"
id hadoop
echo "groups hadoop"
groups hadoop

echo "&&&&&&&&&&&&&&&&&&&&01"
su - hadoop

cd ~
wget https://downloads.apache.org/hadoop/common/hadoop-${HADOOP_VER}hadoop-${HADOOP_VER}.tar.gz
tar xzf hadoop-${HADOOP_VER}.tar.gz
mv hadoop-${HADOOP_VER} hadoop

echo "&&&&&&&&&&&&&&&&&&&&02"
sudo cat "export JAVA_HOME=$JAVA_HOME" >> $HADOOP_HOME/etc/hadoop/hadoop-env.sh

echo "&&&&&&&&&&&&&&&&&&&&04"
## It is required to set up key-based ssh to its own account. To do this use execute following commands:
bash ssh-keygen -t rsa -P '' -f ~/.ssh/id_rsa
cat ~/.ssh/id_rsa.pub >> ~/.ssh/authorized_keys
ls -al ~/.ssh/authorized_keys
echo "chmod 0600 ~/.ssh/authorized_keys"
chmod 0600 ~/.ssh/authorized_keys
echo "ls -al ~/.ssh/authorized_keys"
ls -al ~/.ssh/authorized_keys

echo "&&&&&&&&&&&&&&&&&&&&05"
## Now, SSH to localhost with Hadoop user. 
## This should not ask for the password but the first time 
## it will prompt for adding RSA to the list of known hosts.

bash ssh localhost 

## hadoop??  > /home/${USER_NAME_IN_OS}/KD/_KDPROJ/tmp/LOGS/hadoop_active.log
## echo "tail -f /home/${USER_NAME_IN_OS}/KD/_KDPROJ/tmp/LOGS/hadoop_active.log" >> ./08_02_sudo_kubeadm_join.sh


exit

echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo "********************************************************************************"
