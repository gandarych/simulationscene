#!/bin/bash

## 02_04_add_tomcat_user on ubuntu

echo "pwd =" $(pwd)
echo "USER_NAME_IN_OS = $USER_NAME_IN_OS, hostname =" $(hostname)
echo "date =" $(date)
echo "HOME = $HOME, USER = $USER, LOGNAME = $LOGNAME"
echo "By value: KAFKA_PORT = " $KAFKA_PORT ${KAFKA_PORT} "$KAFKA_PORT ${KAFKA_PORT}"
echo "By value: TOMCAT_VER = " $TOMCAT_VER ${TOMCAT_VER} "$TOMCAT_VER ${TOMCAT_VER}"

if [[ ! -z "$(cut -d= -f1 /home/$USER_NAME_IN_OS/.bashrc | grep declare)" ]]
then
	echo 'Environment variables are set.'
		echo "$(cut -d= -f1 /home/$USER_NAME_IN_OS/.bashrc | grep declare)"
fi

echo "%%%%%%%%%%%%%%%%%%"

pwd
cd $PROJECT_HOME
pwd
echo "RUNNING 02_04_add_tomcat_user.sh"

## add-apt-repository -y = silent execution of this command
## >> /tmp/output.txt
## The will tell Bash to redirect stderr to stdout.
# sudo add-apt-repository -y ppa:fossproject/ppa >> /tmp/output.txt
## Or use output redirection to /dev/null and include stderr output.
# sudo add-apt-repository -y ppa:fossproject/ppa >> /dev/null

## https://medium.com/@madeeshafernando/installing-apache-tomcat-on-ubuntu-18-04-8cf8bc63993d
## https://www.liquidweb.com/kb/how-to-install-apache-tomcat-9-on-ubuntu-18-04/

## tomcat user = user will be the one who has group ownership to the Tomcat files
echo "sudo useradd -r -m -U -d /opt/tomcat -s /bin/false tomcat"
## Creation a non-login user 'tomcat':
sudo useradd -r -m -U -d /opt/tomcat -s /bin/false tomcat
##THE SAME AS:  sudo useradd --system  --create-home --user-group --home-dir /opt/tomcat --shell /bin/false tomcat
##  -r, --system                        create a system account
##  -m, --create-home               create the user's home directory
##  -U, --user-group                  create a group with the same name as the user
##  -d, --home-dir HOME_DIR   home directory of the new account
##  -s, --shell SHELL                login shell of the new account
##  -g, --gid GROUP                 name or ID of the primary group of the new account, if not the same name as the user
sudo usermod -a -G "$MISE_GROUP_NAME" tomcat


## update permissions to Tomcat - for some of the directories of Tomcat.
echo "sudo chown -RH tomcat: /opt/tomcat/apache-tomcat-${TOMCAT_VER}"
sudo chown -RH tomcat: /opt/tomcat/apache-tomcat-${TOMCAT_VER}
echo "sudo chgrp -R tomcat /opt/tomcat"
sudo chgrp -R tomcat /opt/tomcat
echo "cd /opt/tomcat/apache-tomcat-${TOMCAT_VER}"
cd /opt/tomcat/apache-tomcat-${TOMCAT_VER}
echo "sudo chmod -R g+r conf"
sudo chmod -R g+r conf
echo "sudo chmod g+x conf"
sudo chmod g+x conf
echo "sudo chown -R tomcat webapps/ work/ temp/ logs/"
sudo chown -R tomcat webapps/ work/ temp/ logs/


## Copy the path of Tomcat’s home by running this command:
echo "sudo update-java-alternatives -l"
sudo update-java-alternatives -l
## Output:
## java-1.11.0-openjdk-amd64      1101       /usr/lib/jvm/java-1.11.0-openjdk-amd64</b
## take the highlighted path and put it into your /etc/systemd/system/tomcat.service file, as the JAVA_HOME variable (shown below).

echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo "********************************************************************************"
