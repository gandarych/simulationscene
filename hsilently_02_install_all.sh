#!/bin/bash


source ./_ubuntu_DO_ALL/03_02_systemctl_start_tomcat.sh
source ./_ubuntu_DO_ALL/03_03_install_packer.sh
source ./_ubuntu_DO_ALL/03_04_ruby_sinatra.sh
source ./_ubuntu_DO_ALL/04_01_install_docker.sh
source ./_ubuntu_DO_ALL/04_02_using_docker_cmd_AS_ROOT.sh
source ./_ubuntu_DO_ALL/04_03_docker-compose_up.sh
source ./_ubuntu_DO_ALL/05_01_install_ansible_AS_ROOT.sh
source ./_ubuntu_DO_ALL/06_01_install_terraform_AS_ROOT.sh
source ./_ubuntu_DO_ALL/07_01_install_Golang.sh
source ./_ubuntu_DO_ALL/08_01_install_kubernetes.sh

# source ./_ubuntu_DO_ALL/08_02_sudo_kubeadm_join.sh
echo "# source ./_ubuntu_DO_ALL/08_02_sudo_kubeadm_join.sh"

source jsilently_MVN_build_all_deploy_and_run_EN.sh
