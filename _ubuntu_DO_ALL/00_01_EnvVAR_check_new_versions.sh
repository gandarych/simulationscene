#!/bin/bash

echo "date = $(date)"
echo "HOME = $HOME"
echo "USER = $USER"
echo "LOGNAME = $LOGNAME"
hostname
pwd
df
env

echo "%%%%%%%%%%%%%%%%%%% 1 %%%%%%%%%%%%%%%%%%%%%"

echo "LIST APPS to install (from WHAT_TO_INSTALL.txt file):"

if [ "$YES_TO_INSTALL_ANSIBLE" == "Y" ]
then
	echo "ANSIBLE"
fi
if [ "$YES_TO_INSTALL_CONSUL" == "Y" ]
then
	echo "CONSUL"
fi
if [ "$YES_TO_INSTALL_DOCKERCOMPOSE" == "Y" ]
then
	echo "DOCKERCOMPOSE"
fi
if [ "$YES_TO_INSTALL_DOCKER" == "Y" ]
then
	echo "DOCKER"
fi
if [ "$YES_TO_INSTALL_EUREKA" == "Y" ]
then
	echo "EUREKA"
fi
if [ "$YES_TO_INSTALL_GOLANG" == "Y" ]
then
	echo "GOLANG"
fi
if [ "$YES_TO_INSTALL_GRADLE" == "Y" ]
then
	echo "GRADLE"
fi
if [ "$YES_TO_INSTALL_HADOOP" == "Y" ]
then
	echo "HADOOP"
fi
if [ "$YES_TO_INSTALL_HCL" == "Y" ]
then
	echo "HCL"
fi
if [ "$YES_TO_INSTALL_HTOP_GPARTED" == "Y" ]
then
	echo "HTOP_GPARTED"
fi
if [ "$YES_TO_INSTALL_KAFKA" == "Y" ]
then
	echo "KAFKA"
fi
if [ "$YES_TO_INSTALL_KUBERNETES" == "Y" ]
then
	echo "KUBERNETES"
fi
if [ "$YES_TO_INSTALL_MAVEN" == "Y" ]
then
	echo "MAVEN"
fi
if [ "$YES_TO_INSTALL_NET_IDES" == "Y" ]
then
	echo "NET_IDES"
fi
if [ "$YES_TO_INSTALL_NODEJS" == "Y" ]
then
	echo "NODEJS"
fi
if [ "$YES_TO_INSTALL_PACKER" == "Y" ]
then
	echo "PACKER"
fi
if [ "$YES_TO_INSTALL_RUBY_SINATRA" == "Y" ]
then
	echo "RUBY_SINATRA"
fi
if [ "$YES_TO_INSTALL_SCREEN_RECORDERS" == "Y" ]
then
	echo "SCREEN_RECORDERS"
fi
if [ "$YES_TO_INSTALL_TERRAFORM" == "Y" ]
then
	echo "TERRAFORM"
fi
if [ "$YES_TO_INSTALL_TOMCAT" == "Y" ]
then
	echo "TOMCAT"
fi
if [ "$YES_TO_INSTALL_VAULT" == "Y" ]
then
	echo "VAULT"
fi
if [ "$YES_TO_INSTALL_ZOOKEEPER" == "Y" ]
then
	echo "ZOOKEEPER"
fi

echo "%%%%%%%%%%%%%%%%%%% 2 %%%%%%%%%%%%%%%%%%%%%"

echo "CHECK THE LAST VERSION OF:"

if [[ ! -z "$JAVA_VER" ]] 
then
	echo "## ======  01_04_install_openjdk ======"
	echo "## CHECK THE LAST VERSION OF OPEN JDK JAVA"
	echo "JAVA_VER = $JAVA_VER"
	echo "JAVA_HOME = $JAVA_HOME"
fi
if [[ ! -z "$MAVEN_VER" ]] 
then
	echo "## ====== 02_01_install_maven_gradle.sh ======"
	echo "## CHECK THE LAST VERSION AT https://downloads.apache.org/maven/maven-3/ or https://downloads.apache.org/maven/"
	echo "MAVEN_VER = $MAVEN_VER"
	echo "MAVEN_HOME = $MAVEN_HOME"
	echo "MAVEN_RUN = $MAVEN_RUN"
fi
if [[ ! -z "$GRADLE_VER" ]] 
then
	echo "## ====== 02_01_install_maven_gradle.sh ======"
	echo "## CHECK THE LAST VERSION AT https://services.gradle.org/distributions/"
	echo "GRADLE_VER = $GRADLE_VER"
fi
if [[ ! -z "$TOMCAT_VER" ]] 
then
	echo "## ====== 02_03_install_tomcat.sh ======"
	echo "## CHECK THE LAST VERSION AT https://archive.apache.org/dist/tomcat/tomcat-9 OR https://archive.apache.org/dist/tomcat/"
	echo "TOMCAT_VER = $TOMCAT_VER"	
fi
if [[ ! -z "$CONSUL_VER" ]] 
then
	echo "## ====== 02_05_install_discovery_kafka.sh ======"
	echo "## CHECK THE LAST VERSION AT https://releases.hashicorp.com/consul/"
	echo "CONSUL_VER = $CONSUL_VER"
fi
if [[ ! -z "$ZOOKEEPER_VER" ]] 
then
	echo "## ====== 02_05_install_discovery_kafka.sh ======"
	echo "## CHECK THE LAST VERSION AT https://downloads.apache.org/zookeeper/"
	echo "ZOOKEEPER_VER = $ZOOKEEPER_VER"
	echo "ZOOKEEPER_PORT_1 = $ZOOKEEPER_PORT_1"
	echo "ZOOKEEPER_PORT_2 = $ZOOKEEPER_PORT_2"
	echo "ZOOKEEPER_PORT_3 = $ZOOKEEPER_PORT_3"
fi
if [[ ! -z "$KAFKA_VER_1" ]] 
then
	echo "## CHECK THE LAST VERSION AT https://downloads.apache.org/kafka/"
	echo "KAFKA_VER_1 = $KAFKA_VER_1"
	echo "KAFKA_VER_2 = $KAFKA_VER_2"
	echo "KAFKA_PORT = $KAFKA_PORT"
fi
if [[ ! -z "$EUREKA_PORT" ]] 
then
	echo "## CHECK THE LAST VERSION... "
	echo "EUREKA_PORT = $EUREKA_PORT"
	echo "EUREKA_SERVER_NR = $EUREKA_SERVER_NR"
	echo "EUREKA_SERVER_VER = $EUREKA_SERVER_VER"
fi
if [[ ! -z "$VAULT_VER" ]] 
then
	echo "## ====== 02_07_install_secrets.sh ======"
	echo "## CHECK THE LAST VERSION AT https://releases.hashicorp.com/vault/"
	echo "VAULT_VER = $VAULT_VER"
fi
if [[ ! -z "$HADOOP_VER" ]] 
then
	echo "## ====== 02_08_install_hadoop.sh ======"
	echo "## CHECK THE LAST VERSION AT https://downloads.apache.org/hadoop/common/"
	echo "HADOOP_VER = $HADOOP_VER"
	echo "HADOOP1_DEFAULT_PORT = $HADOOP1_DEFAULT_PORT"
	echo "HADOOP2_SERVER_PORT = $HADOOP2_SERVER_PORT"
	echo "HADOOP3_CLUSTER_PORT = $HADOOP3_CLUSTER_PORT"
	echo "HADOOP4_NODE_PORT = $HADOOP4_NODE_PORT"
	
	echo "HADOOP_HOME = $HADOOP_HOME"
	echo "HADOOP_INSTALL = $HADOOP_INSTALL"
	echo "HADOOP_MAPRED_HOME = $HADOOP_MAPRED_HOME"
	echo "HADOOP_COMMON_HOME = $HADOOP_COMMON_HOME"
	echo "HADOOP_HDFS_HOME = $HADOOP_HDFS_HOME"
	echo "YARN_HOME = $YARN_HOME"
	echo "HADOOP_COMMON_LIB_NATIVE_DIR = $HADOOP_COMMON_LIB_NATIVE_DIR"
	echo "PATH = $PATH"
fi

echo "## ====== 03_01_vi_tomcat_service.sh ======"
	echo "## CHECK THE LAST VERSION... "
	echo "CATALINA_BASE = $CATALINA_BASE"
	echo "CATALINA_HOME = $CATALINA_HOME"
	echo "CATALINA_PID = $CATALINA_PID"

if [[ ! -z "$PACKER_VER" ]] 
then
	echo "## ====== 03_03_install_packer.sh ======"
	echo "## CHECK THE LAST VERSION AT https://www.packer.io/downloads.html"
	echo " = $PACKER_VER"
fi

if [[ ! -z "$TERRAF_VER" ]] 
then
	echo "## ====== 06_01_install_terraform_AS_ROOT ======"
	echo "## CHECK THE LAST VERSION AT www.terraform.io/downloads.html"
	echo " = $TERRAF_VER"
fi


### SEE:  96_01_check_started_services_now.sh
### SEE:  97_01_start_services_before_build.sh


echo "###############7################"

echo "pwd = $(pwd)"

echo "env = $(env)"

echo "HOME = $HOME"

echo "###############8################"
