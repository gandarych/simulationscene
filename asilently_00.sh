#!/bin/bash

sudo echo "%%%%%%%%% 10 %%%%%%%%%"

## https://bash.cyberciti.biz/guide/Variables
#set -o allexport
#[[ -f ./_ubuntu_DO_ALL/ENV_VAR.txt ]] && source ./_ubuntu_DO_ALL/ENV_VAR.txt
#set +o allexport

set -a
[ -f ./_ubuntu_DO_ALL/ENV_VAR.txt ] && . ./_ubuntu_DO_ALL/ENV_VAR.txt
set +a


source ./_ubuntu_DO_ALL/ENV_VAR.txt
## 'cut' command: cuts/splits lines on the delimeter (specified by -d) 
## and then selects certain fields from those cut up lines. 
## Which fields, is specified by -f (counting starts at 1, not at 0). 
## See 'man cut': -s, --only-delimited = do not print lines not containing delimiters
export $(cut -d= -f1 ./_ubuntu_DO_ALL/ENV_VAR.txt)

sudo echo "%%%%%%%%% 11 %%%%%%%%%"
sudo echo "sudo USER_NAME_IN_OS = $USER_NAME_IN_OS"

sudo echo "%%%%%%%%% 12 %%%%%%%%%"
sudo echo "USER_NAME_IN_OS = $USER_NAME_IN_OS"

sudo echo "%%%%%%%%% 13 %%%%%%%%%"
if [[ -z "$USER_NAME_IN_OS" ]] 
then
	echo 'ERROR! Failed! Variable $USER_NAME_IN_OS is not set!'
fi
if [[ ! -z "$USER_NAME_IN_OS" ]] 
then
	echo "USER_NAME_IN_OS = $USER_NAME_IN_OS"
	if [[ ! -z "$(cut -d= -f1 /home/$USER_NAME_IN_OS/.bashrc | grep declare)" ]]
	then
		echo 'Environment variables are set.'
		echo "$(cut -d= -f1 /home/$USER_NAME_IN_OS/.bashrc | grep declare)"
	fi
	if [[ -z "$(cut -d= -f1 /home/$USER_NAME_IN_OS/.bashrc | grep declare)" ]] 
	then
		echo 'Setting up!!! Environment variables:'
		sudo echo "$(export | grep -v root | grep -v COLORTERM)" 
		sudo echo "$(export | grep -v root | grep -v COLORTERM)" >> /home/$USER_NAME_IN_OS/.bashrc
	fi
fi

sudo echo "%%%%%%%%% 14 %%%%%%%%%"
source ./_ubuntu_DO_ALL/00_00_env_testing.sh


sudo echo "%%%%%%%%% 15 %%%%%%%%%"
sudo echo "USER_NAME_IN_OS = $USER_NAME_IN_OS"
sudo echo "date =" $(date)
sudo echo "HOME = $HOME, USER = $USER, LOGNAME = $LOGNAME"
sudo echo "hostname =" $(hostname)
sudo echo "pwd =" $(pwd)
sudo echo "By value:" $KAFKA_PORT ${KAFKA_PORT} "$KAFKA_PORT ${KAFKA_PORT}"
sudo echo "By reference:" '$KAFKA_PORT' '${KAFKA_PORT}'
sudo echo "By value:" $TOMCAT_VER ${TOMCAT_VER} "$TOMCAT_VER ${TOMCAT_VER}"
sudo echo "By reference:" '$TOMCAT_VER' '${TOMCAT_VER}'


echo "%%%%%%%%% 16 %%%%%%%%%"
echo "USER_NAME_IN_OS = $USER_NAME_IN_OS"
echo "date =" $(date)
echo "HOME = $HOME, USER = $USER, LOGNAME = $LOGNAME"
echo "hostname =" $(hostname)
echo "pwd =" $(pwd)
echo "By value:" $KAFKA_PORT ${KAFKA_PORT} "$KAFKA_PORT ${KAFKA_PORT}"
echo "By reference:" '$KAFKA_PORT' '${KAFKA_PORT}'
echo "By value:" $TOMCAT_VER ${TOMCAT_VER} "$TOMCAT_VER ${TOMCAT_VER}"
echo "By reference:" '$TOMCAT_VER' '${TOMCAT_VER}'



