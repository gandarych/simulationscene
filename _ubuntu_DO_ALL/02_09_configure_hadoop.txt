
echo "02_08_configure_hadoop.txt"
## Configure Hadoop - Setup Configuration Files

## https://tecadmin.net/setup-hadoop-on-ubuntu/

## export USER_NAME_IN_OS=kris
## export MISE_GROUP_NAME=microservice
## sudo echo "export USER_NAME_IN_OS=kris" >> ~/.bashrc
## sudo echo "export MISE_GROUP_NAME=microservice" >> ~/.bashrc

## Hadoop has many configuration files, which need to configure as per requirements of your Hadoop infrastructure. 
## Let’s start with the configuration with basic Hadoop single node cluster setup. first, navigate to below location

export HADOOP1_DEFAULT_PORT=9000
export HADOOP2_SERVER_PORT=9870
export HADOOP3_CLUSTER_PORT=8042
export HADOOP4_NODE_PORT=9864

cd $HADOOP_HOME/etc/hadoop

## ############# Editing configuration files ###############################

## ---------------------------------- Edit core-site.xml: -----------------------------------
<configuration>
<property>
  <name>fs.default.name</name>
    <value>hdfs://localhost:9000</value>
</property>
</configuration>

## ---------------------------------- Edit hdfs-site.xml: -----------------------------------
Edit hdfs-site.xml

<configuration>
<property>
 <name>dfs.replication</name>
 <value>1</value>
</property>

<property>
  <name>dfs.name.dir</name>
    <value>file:///home/hadoop/hadoopdata/hdfs/namenode</value>
</property>

<property>
  <name>dfs.data.dir</name>
    <value>file:///home/hadoop/hadoopdata/hdfs/datanode</value>
</property>
</configuration>

## ---------------------------------- Edit mapred-site.xml: -----------------------------------
Edit mapred-site.xml

<configuration>
 <property>
  <name>mapreduce.framework.name</name>
   <value>yarn</value>
 </property>
</configuration>

## ---------------------------------- Edit yarn-site.xml: -----------------------------------
Edit yarn-site.xml

<configuration>
 <property>
  <name>yarn.nodemanager.aux-services</name>
    <value>mapreduce_shuffle</value>
 </property>
</configuration>

## ------------------------------------------------------------------------------------------

## Now format the namenode using the following command:
hdfs namenode -format

## ############# Start Hadoop Cluster (checking it in Internet Browser...) ##################################
## Navigate to your $HADOOP_HOME/sbin directory and execute scripts one by one:
cd $HADOOP_HOME/sbin/
./start-dfs.sh
./start-yarn.sh

## Access Hadoop Services in Browser
## Hadoop NameNode started on default port HADOOP2_SERVER_PORT=9870. 

## Access your server on port HADOOP2_SERVER_PORT=9870:
bash /usr/bin/firefox -private -new-window "http://localhost:${HADOOP2_SERVER_PORT}/"

## Getting the information about the cluster and all applications - HADOOP3_CLUSTER_PORT=8042:
bash /usr/bin/firefox -private -new-window "http://localhost:${HADOOP3_CLUSTER_PORT}/"

## Getting details about your Hadoop node - HADOOP4_NODE_PORT=9864:
bash /usr/bin/firefox -private -new-window "http://localhost:${HADOOP4_NODE_PORT}/"

## ############# Test Hadoop Single Node Setup ###############################

## Make the HDFS directories required using following commands:

bin/hdfs dfs -mkdir /user
bin/hdfs dfs -mkdir /user/hadoop

## Copy all files from local file system /var/log/httpd to hadoop distributed file system using below command:

bin/hdfs dfs -put /var/log/apache2 logs

## Browse Hadoop distributed file system by opening below URL in the browser. 
## You will see an apache2 folder in the list. 
## Click on the folder name to open and you will find all log files there:

 http://localhost:9870/explorer.html#/user/hadoop/logs/

## Now copy logs directory for hadoop distributed file system to local file system.

bin/hdfs dfs -get logs /tmp/logs
ls -l /tmp/logs/

## You can also check this tutorial to run wordcount mapreduce job example using command line...
## https://tecadmin.net/hadoop-running-a-wordcount-mapreduce-example/

## https://expertlinux.eu/2019/01/17/powiekszanie-partycji-lvm/

echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo "********************************************************************************"

