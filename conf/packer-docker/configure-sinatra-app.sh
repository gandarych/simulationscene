#!/bin/bash
# Install and configure a simple web app built on top of Ruby and Sinatra

set -e

readonly APP_RB_SRC="/tmp/packer-docker/app.rb"
readonly APP_RB_DST="/home/kris/KD/_KDPROJ/tmp/packer-docker/app.rb"

echo "Installing Ruby"
sudo apt-get update
sudo apt-get install -y make zlib1g-dev build-essential ruby ruby-dev

echo "Installing Sinatra"
sudo apt install -y gem
sudo gem install sinatra json

echo "Moving $APP_RB_SRC to $APP_RB_DST"
mkdir -p "$(dirname "$APP_RB_DST")"
cp -pr "$APP_RB_SRC" "$APP_RB_DST"
