CONSUL_VER=1.7.2
EUREKA_PORT=8761
EUREKA_SERVER_NR=1.9.22
EUREKA_SERVER_VER=-server-$EUREKA_SERVER_NR
GRADLE_VER=6.4
HADOOP1_DEFAULT_PORT=9000
HADOOP2_SERVER_PORT=9870
HADOOP3_CLUSTER_PORT=8042
HADOOP4_NODE_PORT=9864
HADOOP_COMMON_HOME=$HADOOP_HOME
HADOOP_COMMON_LIB_NATIVE_DIR=$HADOOP_HOME/lib/native
HADOOP_HDFS_HOME=$HADOOP_HOME
HADOOP_HOME=/opt/hadoop/hadoop
HADOOP_INSTALL=$HADOOP_HOME
HADOOP_MAPRED_HOME=$HADOOP_HOME
HADOOP_VER=3.2.1
JAVA_HOME=/usr/lib/jvm/java-$JAVA_VER-openjdk-amd64
JAVA_VER=8
KAFKA_PORT=9092
KAFKA_VER_1=2.12
KAFKA_VER_2=2.5.0
MAVEN_HOME=/opt/maven/maven-$MAVEN_VER
MAVEN_ON_AND_GRADLE_OFF=Y
MAVEN_RUN=$MAVEN_HOME/bin/mvn
MAVEN_VER=3.6.3
MISE_GROUP_NAME=microservice
PACKER_VER=1.5.5
PATH=$PATH:$HADOOP_HOME/sbin:$HADOOP_HOME/bin
PROJECT_HOME=$PWD
TERRAF_VER=0.12.24
TOMCAT_VER=9.0.33
USER_NAME_IN_OS=kris
VAULT_VER=1.4.0
YARN_HOME=$HADOOP_HOME
ZOOKEEPER_PORT_1=2181
ZOOKEEPER_PORT_2=2182
ZOOKEEPER_PORT_3=2183
ZOOKEEPER_VER=3.6.1

