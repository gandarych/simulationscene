#!/bin/bash

#!/bin/bash


#set -o allexport
#[[ -f ./_ubuntu_DO_ALL/env.txt ]] && source ./_ubuntu_DO_ALL/env.txt
#set +o allexport


set -a
[ -f ./_ubuntu_DO_ALL/env.txt ] && . ./_ubuntu_DO_ALL/env.txt
set +a



source ./_ubuntu_DO_ALL/env.txt
export $(cut -d= -f1 ./_ubuntu_DO_ALL/env.txt)
## source ./_ubuntu_DO_ALL/00_01_EnvVAR_check_new_versions.sh

source ./_ubuntu_DO_ALL/01_02_update_ubuntu.sh

source ./_ubuntu_DO_ALL/02_01_install_openjdk.sh

source ./_ubuntu_DO_ALL/02_02_install_maven.sh

source ./_ubuntu_DO_ALL/02_03_install_tomcat.sh

source ./_ubuntu_DO_ALL/02_04_add_tomcat_user.sh

## Modify the content of tomcat.service
cat ./_ubuntu_DO_ALL/03_01_vi_tomcat_service.txt

## source ./_ubuntu_DO_ALL/03_02_systemctl_start_tomcat.sh
