#!/bin/bash

###### 03_04_ruby_sinatra on ubuntu

echo "pwd =" $(pwd)
echo "USER_NAME_IN_OS = $USER_NAME_IN_OS, hostname =" $(hostname)
echo "date =" $(date)
echo "HOME = $HOME, USER = $USER, LOGNAME = $LOGNAME"
echo "By value: KAFKA_PORT = " $KAFKA_PORT ${KAFKA_PORT} "$KAFKA_PORT ${KAFKA_PORT}"
echo "By value: TOMCAT_VER = " $TOMCAT_VER ${TOMCAT_VER} "$TOMCAT_VER ${TOMCAT_VER}"

if [[ ! -z "$(cut -d= -f1 /home/$USER_NAME_IN_OS/.bashrc | grep declare)" ]]
then
	echo 'Environment variables are set.'
		echo "$(cut -d= -f1 /home/$USER_NAME_IN_OS/.bashrc | grep declare)"
fi

echo "%%%%%%%%%%%%%%%%%%"

pwd
cd $PROJECT_HOME
pwd
echo "RUNNING 03_04_ruby_sinatra.sh"

## add-apt-repository -y = silent execution of this command
## >> /tmp/output.txt
## The will tell Bash to redirect stderr to stdout.
# sudo add-apt-repository -y ppa:fossproject/ppa >> /tmp/output.txt
## Or use output redirection to /dev/null and include stderr output.
# sudo add-apt-repository -y ppa:fossproject/ppa >> /dev/null
# Install and configure a simple web app built on top of Ruby and Sinatra

set -e

# readonly APP_RB_SRC="/tmp/packer-docker/app.rb"
# readonly APP_RB_DST="/home/${USER_NAME_IN_OS}/KD/_KDPROJ/tmp/packer-docker/app.rb"

export  APP_RB_SRC="/tmp/packer-docker/app.rb"
export  APP_RB_DST="/home/$USER_NAME_IN_OS/KD/_KDPROJ/tmp/packer-docker/app.rb"
sudo echo "APP_RB_SRC=$APP_RB_SRC" >> ~/.bashrc
sudo echo "APP_RB_DST=$APP_RB_DST" >> ~/.bashrc
sudo echo "APP_RB_SRC=$APP_RB_SRC" >> /root/.bashrc
sudo echo "APP_RB_DST=$APP_RB_DST" >> /root/.bashrc

source ~/.bashrc


echo "&&&&&&&&&&&&&&&&&&&&01"
echo "Installing Ruby"

sudo echo "deb http://us.archive.ubuntu.com/ubuntu/ bionic main universe" >> /etc/apt/sources.list
sudo echo "deb-src http://us.archive.ubuntu.com/ubuntu/ bionic main universe" >> /etc/apt/sources.list

echo "&&&&&&&&&&&&&&&&&&&&02"
sudo apt-get update && sudo apt-get install -y libesd0-dev


echo "&&&&&&&&&&&&&&&&&&&&03"
sudo apt install -y nodejs

echo "&&&&&&&&&&&&&&&&&&&&04"
sudo add-apt-repository -y universe
sudo add-apt-repository -y multiverse
sudo apt install -y gnupg2
## https://packages.ubuntu.com/bionic/

echo "&&&&&&&&&&&&&&&&&&&&05"
## Import the signing key to be able to verify the RVM packages downloaded in the later step
gpg2 --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3 7D2BAF1CF37B13E2069D6956105BD0E739499BDB

echo "&&&&&&&&&&&&&&&&&&&&06"
# sudo apt install -y curl
curl -sSL https://get.rvm.io | bash -s stable --ruby

source /usr/local/rvm/scripts/rvm
echo "&&&&&&&&&&&&&&&&&&&&07"
rvm requirements
rvm install ruby
echo "&&&&&&&&&&&&&&&&&&&&08"
ruby --version
gem install bundler
rvm use ruby --default
ruby --version
rvm rubygems current


echo "&&&&&&&&&&&&&&&&&&&&06"
sudo apt-get install -y make zlib1g-dev build-essential ruby ruby-dev


echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo "Installing Sinatra"
sudo apt install -y gem
sudo gem install sinatra json

## http://sinatrarb.com/intro.html
## myapp.rb
#require 'sinatra'
#
#get '/' do
#  'Hello world!'
#end

## https://www.twilio.com/docs/usage/tutorials/how-to-set-up-your-ruby-and-sinatra-development-environment

ruby --version

echo "Moving $APP_RB_SRC to $APP_RB_DST"
mkdir -p "$(dirname "$APP_RB_DST")"
cp -pr "$APP_RB_SRC" "$APP_RB_DST"


## https://www.digitalocean.com/community/tutorials/how-to-install-ruby-2-1-0-and-sinatra-on-ubuntu-13-with-rvm
## https://www.digitalocean.com/community/tutorials/how-to-install-and-get-started-with-sinatra-on-your-system-or-vps
sudo addgroup www

echo "sudo useradd -r -m -U -d /opt/deployer -s /bin/false deployer"
## Creation a non-login user 'deployer':
sudo useradd -r -m -U -d /opt/deployer -s /bin/false -g www deployer
##  -r, --system                        create a system account
##  -m, --create-home               create the user's home directory
##  -U, --user-group                  create a group with the same name as the user
##  -d, --home-dir HOME_DIR   home directory of the new account
##  -s, --shell SHELL                login shell of the new account
##  -g, --gid GROUP                 name or ID of the primary group of the new account, if not the same name as the user

sudo usermod -a -G "$MISE_GROUP_NAME" deployer

echo "id deployer"
id deployer
echo "groups deployer"
groups deployer

echo "sudo mkdir /var/www"
sudo mkdir /var/www

# Set the ownership of the folder to members of `www` group
echo "sudo chown -R :www  /var/www"
sudo chown -R :www  /var/www

# Set folder permissions recursively
echo "sudo chmod -R g+rwX /var/www"
sudo chmod -R g+rwX /var/www

# Ensure permissions will affect future sub-directories etc.
echo "sudo chmod g+s   /var/www"
sudo chmod g+s   /var/www

## sudo echo "deployer ALL=(ALL:ALL) ALL" >> /etc/sudoers

## .....

echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo "********************************************************************************"
