CREATE TABLE simulation (
	id INT PRIMARY KEY AUTO_INCREMENT,
    	name VARCHAR(255), 
	score DOUBLE,
	userid INT,
	visitorid INT,
	worldid INT
);

CREATE TABLE exercise (
	id INT PRIMARY KEY AUTO_INCREMENT,
    	simulationid INT,
	attendantid INT
);

INSERT INTO simulation (id, name, score, userid, visitorid, worldid) VALUES (1, 'Good game 1', 20.0, 1, 1, 1); 
INSERT INTO simulation (id, name, score, userid, visitorid, worldid) VALUES (2, 'Testing castle 1', 5.0, 1, 2, 1); 
INSERT INTO simulation (id, name, score, userid, visitorid, worldid) VALUES (3, 'TOC', 10.0, 1, 3, 2); 
INSERT INTO simulation (id, name, score, userid, visitorid, worldid) VALUES (4, 'etc', 35.0, 1, 2, 1); 
INSERT INTO simulation (id, name, score, userid, visitorid, worldid) VALUES (5, 'Good game 2', 80.0, 1, 1, 1); 
INSERT INTO simulation (id, name, score, userid, visitorid, worldid) VALUES (6, 'My simula 1', 15.0, 1, 2, 2); 
INSERT INTO simulation (id, name, score, userid, visitorid, worldid) VALUES (7, 'My simula 1', 30.0, 1, 3, 1); 
INSERT INTO simulation (id, name, score, userid, visitorid, worldid) VALUES (8, 'My simula 1', 75.0, 1, 1, 2); 

INSERT INTO exercise (id, simulationid, attendantid) VALUES (1, 1, 1); 
INSERT INTO exercise (id, simulationid, attendantid) VALUES (2, 2, 2); 
INSERT INTO exercise (id, simulationid, attendantid) VALUES (3, 3, 3); 
INSERT INTO exercise (id, simulationid, attendantid) VALUES (4, 4, 2); 
INSERT INTO exercise (id, simulationid, attendantid) VALUES (5, 5, 1); 
INSERT INTO exercise (id, simulationid, attendantid) VALUES (6, 6, 2); 
INSERT INTO exercise (id, simulationid, attendantid) VALUES (7, 7, 3); 
INSERT INTO exercise (id, simulationid, attendantid) VALUES (8, 8, 1); 

INSERT INTO exercise (id, simulationid, attendantid) VALUES (9, 1, 1); 
INSERT INTO exercise (id, simulationid, attendantid) VALUES (10, 2, 1); 
INSERT INTO exercise (id, simulationid, attendantid) VALUES (11, 3, 1); 
INSERT INTO exercise (id, simulationid, attendantid) VALUES (12, 4, 1); 
INSERT INTO exercise (id, simulationid, attendantid) VALUES (13, 5, 1); 
INSERT INTO exercise (id, simulationid, attendantid) VALUES (14, 6, 1); 
INSERT INTO exercise (id, simulationid, attendantid) VALUES (15, 7, 1); 
INSERT INTO exercise (id, simulationid, attendantid) VALUES (16, 8, 1); 

INSERT INTO exercise (id, simulationid, attendantid) VALUES (17, 1, 2); 
INSERT INTO exercise (id, simulationid, attendantid) VALUES (18, 2, 2); 
INSERT INTO exercise (id, simulationid, attendantid) VALUES (19, 3, 3); 
INSERT INTO exercise (id, simulationid, attendantid) VALUES (20, 4, 2); 
INSERT INTO exercise (id, simulationid, attendantid) VALUES (21, 5, 2); 
INSERT INTO exercise (id, simulationid, attendantid) VALUES (22, 6, 2); 
INSERT INTO exercise (id, simulationid, attendantid) VALUES (23, 7, 3); 
INSERT INTO exercise (id, simulationid, attendantid) VALUES (24, 8, 3); 



