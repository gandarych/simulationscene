#!/bin/bash

###### 06_01_install_terraform_AS_ROOT on ubuntu WHEN WE WORK AS ROOT

echo "pwd =" $(pwd)
echo "USER_NAME_IN_OS = $USER_NAME_IN_OS, hostname =" $(hostname)
echo "date =" $(date)
echo "HOME = $HOME, USER = $USER, LOGNAME = $LOGNAME"
echo "By value: KAFKA_PORT = " $KAFKA_PORT ${KAFKA_PORT} "$KAFKA_PORT ${KAFKA_PORT}"
echo "By value: TOMCAT_VER = " $TOMCAT_VER ${TOMCAT_VER} "$TOMCAT_VER ${TOMCAT_VER}"

if [[ ! -z "$(cut -d= -f1 /home/$USER_NAME_IN_OS/.bashrc | grep declare)" ]]
then
	echo 'Environment variables are set.'
		echo "$(cut -d= -f1 /home/$USER_NAME_IN_OS/.bashrc | grep declare)"
fi

echo "%%%%%%%%%%%%%%%%%%"

pwd
cd $PROJECT_HOME
pwd
echo "RUNNING 06_01_install_terraform_AS_ROOT.sh"

# docker run -it ubuntu bash

# Your command prompt should change to reflect the fact that you're now working inside the container:

## OUTPUT:
#kris@gandalf1:~$     docker run -it ubuntu bash
#root@dfffe6a3e74d:/#

# Pay attention! Note the container ID in the command prompt: it is dfffe6a3e74d.
# You'll need that container ID later to identify the container when you want to remove it.


# Now you can run any command inside the container:
echo "apt update"
apt update

# Any changes you make inside the container only apply to that container.
# To exit the container, type "exit" at the prompt.
echo "snap install terraform"
snap install terraform
## apt install -y terraform
## apt install pip
## pip install terraform
echo "terraform --version"
terraform --version

echo "&&&&&&&&&&&&&&&&&&&&&&&&&&&&&1"
echo "terraform --help"
terraform --help

echo "&&&&&&&&&&&&&&&&&&&&&&&&&&&&&2"
cd /tmp
pwd
wget https://releases.hashicorp.com/terraform/${TERRAF_VER}/terraform_${TERRAF_VER}_linux_amd64.zip
unzip terraform_${TERRAF_VER}_linux_amd64.zip
sudo mv terraform /usr/bin
export "PATH=$PATH:/usr/bin/terraform"
sudo echo 'PATH=$PATH:/usr/bin/terraform' >> ~/.bashrc
source ~/.bashrc

echo "&&&&&&&&&&&&&&&&&&&&&&&&&&&&&3"
terraform --version
terraform --help


##  ping 192.168.1.22
## telnet 192.168.1.22 22
## telnet 192.168.1.22 13531 

####   https://github.com/startup-systems/terraform-ansible-example
####   https://www.hashicorp.com/resources/ansible-terraform-better-together
####   https://alex.dzyoba.com/blog/terraform-ansible/
####   https://medium.com/faun/building-repeatable-infrastructure-with-terraform-and-ansible-on-aws-3f082cd398ad
####   https://victorops.com/blog/writing-ansible-playbooks-for-new-terraform-servers
####   https://getintodevops.com/blog/using-ansible-with-terraform
####   https://www.redhat.com/cms/managed-files/pa-terraform-and-ansible-overview-f14774wg-201811-en.pdf
####   https://stackshare.io/stackups/ansible-vs-terraform    (LOGO, following)

####   Packer provisioner runs Ansible playbooks!!!:
####   https://packer.io/docs/provisioners/ansible.html

echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo "********************************************************************************"
