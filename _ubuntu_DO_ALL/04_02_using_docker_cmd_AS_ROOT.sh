#!/bin/bash

## 04_02_using_docker_cmd_AS_ROOT on ubuntu WHEN WE WORK AS ROOT

echo "pwd =" $(pwd)
echo "USER_NAME_IN_OS = $USER_NAME_IN_OS, hostname =" $(hostname)
echo "date =" $(date)
echo "HOME = $HOME, USER = $USER, LOGNAME = $LOGNAME"
echo "By value: KAFKA_PORT = " $KAFKA_PORT ${KAFKA_PORT} "$KAFKA_PORT ${KAFKA_PORT}"
echo "By value: TOMCAT_VER = " $TOMCAT_VER ${TOMCAT_VER} "$TOMCAT_VER ${TOMCAT_VER}"

if [[ ! -z "$(cut -d= -f1 /home/$USER_NAME_IN_OS/.bashrc | grep declare)" ]]
then
	echo 'Environment variables are set.'
		echo "$(cut -d= -f1 /home/$USER_NAME_IN_OS/.bashrc | grep declare)"
fi

echo "%%%%%%%%%%%%%%%%%%"

pwd
cd $PROJECT_HOME
pwd
echo "RUNNING 04_02_using_docker_cmd_AS_ROOT.sh"

echo "############### Using the DOCKER Command ######################"

# Checking the confirmation that the user is now added to the docker group by typing:
echo "id -nG"
id -nG

# Sample output:   kris adm cdrom sudo dip plugdev lpadmin sambashare docker


## (Optional) If you need to add a user to the docker group that you're not logged in as, declare that username explicitly using:
##   sudo usermod -aG docker username

## The syntax is "a chain of options and commands followed by arguments". Usage:
##docker [OPTIONS] COMMAND [arguments]

# Viewing the version:
echo "docker --version"
docker --version

# Viewing all available subcommands:
echo "docker"
docker

echo "&&&&&&&&&&&&&&&&&&&&01"

# Runing 'docker COMMAND --help' for more information on a specific command:
echo "docker kill --help"
docker kill --help

# Viewing system-wide information about Docker. We can use:
echo "docker info"
docker info

echo "&&&&&&&&&&&&&&&&&&&&02"
echo "############### Working with DOCKER Images ######################"

# Checking if we can access and download images from Docker Hub. We can type:
echo "docker run hello-world"
docker run hello-world

# We can search for images available on Docker Hub by typing:
echo "docker search ubuntu"
docker search ubuntu

echo "&&&&&&&&&&&&&&&&&&&&03"
# Executing the following command to download the official "ubuntu" image to our machine:
echo "docker pull ubuntu"
docker pull ubuntu

# Viewing the images that have been downloaded to our machine. We can type:
echo "docker images"
docker images

echo "&&&&&&&&&&&&&&&&&&&&04"
echo "############### Running a DOCKER Container ######################"

# The "hello-world" container is an example container, which runs and exits after emitting a test message.
# Containers can be much more useful than that, and they can be interactive, as a virtual machines.

# As an second example, let's run a container using the latest official image of "Ubuntu".
# The combination of the -i and -t switches gives you interactive shell access into the container. We can use "bash":
echo "docker run -it ubuntu bash"
echo "Type 'exit' to go outside the dock !!!!!!!!!!!!"
docker run -it ubuntu bash

# Your command prompt should change to reflect the fact that you're now working inside the container:

## OUTPUT:

#kris@gandalf1:~$ docker run -it ubuntu bash
#root@5495e623ad66:/#

# Pay attention! Note the container ID in the command prompt: it is 5495e623ad66.
# You'll need that container ID later to identify the container when you want to remove it.

# Now you can run any command inside the container:
echo "apt update"
apt update

echo "&&&&&&&&&&&&&&&&&&&&05"
# Installing any application in it. Let's install the Node.js:
echo "apt install -y nodejs"
apt install -y nodejs

# It installs Node.js in the container from the official Ubuntu repository.
# "Do you want to continue? [Y/n]"
### >> type "y" + ENTER
# Verify that Node.js is installed to see the version number:
echo "node -v"
node -v

# Any changes you make inside the container only apply to that container.
# To exit the container, type "exit" at the prompt.
echo "exit"
exit

echo "&&&&&&&&&&&&&&&&&&&&06"
echo "############### Managing DOCKER Containers ######################"

# Viewing all containers: active and inactive. Type with the "-a" switch:
echo "docker ps -a"
docker ps -a

# Viewing the latest container you created:
echo "docker ps -l"
docker ps -l

echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo "********************************************************************************"
