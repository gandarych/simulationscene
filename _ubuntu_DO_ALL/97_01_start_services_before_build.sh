#!/bin/bash

## Setting environment variables:
echo "pwd =" $(pwd)
echo "USER_NAME_IN_OS = $USER_NAME_IN_OS, hostname =" $(hostname)
echo "date =" $(date)
echo "HOME = $HOME, USER = $USER, LOGNAME = $LOGNAME"
echo "By value: KAFKA_PORT = " $KAFKA_PORT ${KAFKA_PORT} "$KAFKA_PORT ${KAFKA_PORT}"
echo "By value: TOMCAT_VER = " $TOMCAT_VER ${TOMCAT_VER} "$TOMCAT_VER ${TOMCAT_VER}"

if [[ ! -z "$(cut -d= -f1 /home/$USER_NAME_IN_OS/.bashrc | grep declare)" ]]
then
	echo 'Environment variables are set.'
		echo "$(cut -d= -f1 /home/$USER_NAME_IN_OS/.bashrc | grep declare)"
fi

echo "%%%%%%%%%%%%%%%%%%"

pwd
cd $PROJECT_HOME
pwd

source gnome-terminal &
gnome-terminal &
gnome-terminal &
gnome-terminal &
gnome-terminal &
gnome-terminal &
gnome-terminal &
gnome-terminal &


echo "################### CHECK WHAT IS STARTED NOW ###################"
pwd
echo "${PROJECT_HOME}"
cd "$PROJECT_HOME"
pwd
source ./_ubuntu_DO_ALL/96_01_check_started_services_now.sh

echo "################### Start services!! ###################"
## Start each service in separate process in different terminal to log to a log file
gnome-terminal &
source ./_ubuntu_DO_ALL/00_01_EnvVAR_check_new_versions.sh
echo "----------------------- # 2181 Zookeeper # ----------------------------"
cd /opt/z1/apache-zookeeper-${ZOOKEEPER_VER}-bin/
touch zookeeper1_active.log
sudo mv zookeeper1_active.log /opt/z1/apache-zookeeper-${ZOOKEEPER_VER}-bin/logs/
touch zookeeper2_active.log
sudo mv zookeeper2_active.log /opt/z2/apache-zookeeper-${ZOOKEEPER_VER}-bin/logs/
touch zookeeper3_active.log
sudo mv zookeeper3_active.log /opt/z3/apache-zookeeper-${ZOOKEEPER_VER}-bin/logs/
sudo  java -cp ./lib/zookeeper-${ZOOKEEPER_VER}.jar:lib/*:conf org.apache.zookeeper.server.quorum.QuorumPeerMain /opt/z1/apache-zookeeper-${ZOOKEEPER_VER}-bin/conf/zoo.cfg | tee -a ./logs/zookeeper1.log & 
# java -cp zookeeper.jar:lib/*:conf org.apache.zookeeper.server.quorum.QuorumPeerMain zoo.cfg
cd /opt/z1/apache-zookeeper-${ZOOKEEPER_VER}-bin/
source ./bin/zkCli.sh -server 127.0.0.1:$ZOOKEEPER_PORT_1





gnome-terminal &
source ./_ubuntu_DO_ALL/00_01_EnvVAR_check_new_versions.sh
cd /opt/z1/apache-zookeeper-${ZOOKEEPER_VER}-bin/
source ./bin/zkCli.sh -server 127.0.0.1:$ZOOKEEPER_PORT_2

gnome-terminal &
source ./_ubuntu_DO_ALL/00_01_EnvVAR_check_new_versions.sh
cd /opt/z1/apache-zookeeper-${ZOOKEEPER_VER}-bin/
source ./bin/zkCli.sh -server 127.0.0.1:$ZOOKEEPER_PORT_3


## This can be run as a cron job on the ZooKeeper server machines to clean up the logs daily.
# java -cp zookeeper.jar:lib/slf4j-api-1.7.5.jar:lib/slf4j-log4j12-1.7.5.jar:lib/log4j-1.2.17.jar:conf org.apache.zookeeper.server.PurgeTxnLog <dataDir> <snapDir> -n <count>
pwd
echo "&&&&&&&&&&&&&&&&&&&&02"
## https://medium.com/@ryannel/installing-zookeeper-on-ubuntu-9f1f70f22e25
## Debug Zookeeper:
## The best way to debug Zookeeper is to run it in the foreground so you can watch the output directly.

gnome-terminal &
source ./_ubuntu_DO_ALL/00_01_EnvVAR_check_new_versions.sh
source /opt/z1/apache-zookeeper-${ZOOKEEPER_VER}-bin/bin/zkServer.sh start-foreground
source /opt/z2/apache-zookeeper-${ZOOKEEPER_VER}-bin/bin/zkServer.sh start-foreground
source /opt/z3/apache-zookeeper-${ZOOKEEPER_VER}-bin/bin/zkServer.sh start-foreground
sudo chmod 0775 /home/$USER_NAME_IN_OS/KD/_KDPROJ/tmp/LOGS
touch zookeeper1_active.log
sudo mv zookeeper_active.log /home/${USER_NAME_IN_OS}/KD/_KDPROJ/tmp/LOGS
touch zookeeper2_active.log
sudo mv zookeeper2_active.log /home/${USER_NAME_IN_OS}/KD/_KDPROJ/tmp/LOGS
touch zookeeper3_active.log
sudo mv zookeeper3_active.log /home/${USER_NAME_IN_OS}/KD/_KDPROJ/tmp/LOGS
##source /opt/z1/apache-zookeeper-${ZOOKEEPER_VER}-bin/bin/zkServer.sh start > "/home/${USER_NAME_IN_OS}/KD/_KDPROJ/tmp/LOGS/zookeeper1_active.log"
sudo su -p - zookeeper -c "/opt/z1/apache-zookeeper-${ZOOKEEPER_VER}-bin/bin/zkServer.sh start" > "/home/${USER_NAME_IN_OS}/KD/_KDPROJ/tmp/LOGS/zookeeper1_active.log"


gnome-terminal &
source ./_ubuntu_DO_ALL/00_01_EnvVAR_check_new_versions.sh
tail -f /home/${USER_NAME_IN_OS}/KD/_KDPROJ/tmp/LOGS/zookeeper1_active.log


gnome-terminal &
source ./_ubuntu_DO_ALL/00_01_EnvVAR_check_new_versions.sh
##source /opt/z2/apache-zookeeper-${ZOOKEEPER_VER}-bin/bin/zkServer.sh start > "/home/${USER_NAME_IN_OS}/KD/_KDPROJ/tmp/LOGS/zookeeper2_active.log"
sudo su -p - zookeeper -c "/opt/z2/apache-zookeeper-${ZOOKEEPER_VER}-bin/bin/zkServer.sh start" > "/home/${USER_NAME_IN_OS}/KD/_KDPROJ/tmp/LOGS/zookeeper2_active.log"


gnome-terminal &
source ./_ubuntu_DO_ALL/00_01_EnvVAR_check_new_versions.sh
tail -f /home/${USER_NAME_IN_OS}/KD/_KDPROJ/tmp/LOGS/zookeeper2_active.log


gnome-terminal &
source ./_ubuntu_DO_ALL/00_01_EnvVAR_check_new_versions.sh
##source /opt/z3/apache-zookeeper-${ZOOKEEPER_VER}-bin/bin/zkServer.sh start > "/home/${USER_NAME_IN_OS}/KD/_KDPROJ/tmp/LOGS/zookeeper3_active.log"
sudo su -p - zookeeper -c "/opt/z3/apache-zookeeper-${ZOOKEEPER_VER}-bin/bin/zkServer.sh start" > "/home/${USER_NAME_IN_OS}/KD/_KDPROJ/tmp/LOGS/zookeeper3_active.log"


gnome-terminal &
source ./_ubuntu_DO_ALL/00_01_EnvVAR_check_new_versions.sh
tail -f /home/${USER_NAME_IN_OS}/KD/_KDPROJ/tmp/LOGS/zookeeper3_active.log


gnome-terminal &
source ./_ubuntu_DO_ALL/00_01_EnvVAR_check_new_versions.sh
sudo netstat -ao | grep -i $ZOOKEEPER_PORT_1
sudo netstat -ao | grep -i $ZOOKEEPER_PORT_2
sudo netstat -ao | grep -i $ZOOKEEPER_PORT_3
ps -ef | grep zookeeper
source /opt/z1/apache-zookeeper-${ZOOKEEPER_VER}-bin/bin/zkServer.sh status
source /opt/z2/apache-zookeeper-${ZOOKEEPER_VER}-bin/bin/zkServer.sh status
source /opt/z3/apache-zookeeper-${ZOOKEEPER_VER}-bin/bin/zkServer.sh status

echo "tail -f /home/${USER_NAME_IN_OS}/KD/_KDPROJ/tmp/LOGS/zookeeper1_active.log" >> $PROJECT_HOME/_ubuntu_DO_ALL/08_02_sudo_kubeadm_join.sh
echo "tail -f /home/${USER_NAME_IN_OS}/KD/_KDPROJ/tmp/LOGS/zookeeper2_active.log" >> $PROJECT_HOME/_ubuntu_DO_ALL/08_02_sudo_kubeadm_join.sh
echo "tail -f /home/${USER_NAME_IN_OS}/KD/_KDPROJ/tmp/LOGS/zookeeper3_active.log" >> $PROJECT_HOME/_ubuntu_DO_ALL/08_02_sudo_kubeadm_join.sh



echo "----------------------- # 8000 DEBUG # ----------------------------"
sudo netstat -ao | grep -i "8000"
ps -ef | grep debug




echo "----------------------- # 8080 Tomcat # ----------------------------"
gnome-terminal &
source ./_ubuntu_DO_ALL/00_01_EnvVAR_check_new_versions.sh
echo "tail -f /home/${USER_NAME_IN_OS}/KD/_KDPROJ/tmp/LOGS/tomcat_active.log" | tee -a $PROJECT_HOME/_ubuntu_DO_ALL/08_02_sudo_kubeadm_join.sh
echo "sudo systemctl start tomcat"
echo "Type 'q' !!!!!!!!!!!!"
sudo systemctl start tomcat
#sudo systemctl start tomcat > "/home/${USER_NAME_IN_OS}/KD/_KDPROJ/tmp/LOGS/tomcat_active.log"
echo "q"

gnome-terminal &
source ./_ubuntu_DO_ALL/00_01_EnvVAR_check_new_versions.sh
tail -f /home/${USER_NAME_IN_OS}/KD/_KDPROJ/tmp/LOGS/tomcat_active.log

gnome-terminal &
echo "sudo systemctl status tomcat"
sudo systemctl status tomcat
sudo netstat -ao | grep -i "8080"
ps -ef | grep tomcat




echo "----------------------- # GOTO:  98_01_build_and_start_projects.sh # ----------------------------"
echo "----------------------- # Running apps at ports 8081-8087 # ----------------------------"




echo "----------------------- # 8200 Vault # ----------------------------"




sudo netstat -ao | grep -i "8200" 
ps -ef | grep vault





echo "----------------------- # 8500 Consul # ----------------------------"
source ./_ubuntu_DO_ALL/00_01_EnvVAR_check_new_versions.sh
sudo chmod 0775 /home/${USER_NAME_IN_OS}/KD/_KDPROJ/tmp/LOGS
touch consul_active.log
sudo mv consul_active.log /home/${USER_NAME_IN_OS}/KD/_KDPROJ/tmp/LOGS
consul agent -dev -node machine > "/home/${USER_NAME_IN_OS}/KD/_KDPROJ/tmp/LOGS/consul_active.log"
echo "!!!!!!!!!!!!!!!!!!!!!!!"
echo 'consul agent -dev -node machine > "/home/${USER_NAME_IN_OS}/KD/_KDPROJ/tmp/LOGS/consul_active.log"' | tee -a $PROJECT_HOME/_ubuntu_DO_ALL/08_02_sudo_kubeadm_join.sh
echo "tail -f /home/${USER_NAME_IN_OS}/KD/_KDPROJ/tmp/LOGS/tomcat_active.log" | tee -a $PROJECT_HOME/_ubuntu_DO_ALL/08_02_sudo_kubeadm_join.sh

gnome-terminal &
source ./_ubuntu_DO_ALL/00_01_EnvVAR_check_new_versions.sh
sudo netstat -ao | grep -i "8500"
ps -ef | grep consul
tail -f /home/${USER_NAME_IN_OS}/KD/_KDPROJ/tmp/LOGS/consul_active.log





echo "----------------------- # 8042 Hadoop1_3 !!! # ----------------------------"




sudo netstat -ao | grep -i "8042"
ps -ef | grep Hadoop

echo "----------------------- # 8761 Eureka # ----------------------------"




sudo netstat -ao | grep -i "8761"
ps -ef | grep eureka

echo "----------------------- # 9000 Hadoop1_1 !!! # ----------------------------"




sudo netstat -ao | grep -i "9000"
ps -ef | grep hadoop



echo "----------------------- # 9092 Kafka # ----------------------------"

gnome-terminal &
source ./_ubuntu_DO_ALL/00_01_EnvVAR_check_new_versions.sh
## The next step is to start Kafka server, you can start it by running kafka-server-start.sh script 
## located at /opt/kafka/kafka_${KAFKA_VER_1}-${KAFKA_VER_2}/bin/ directory.
sudo  /opt/kafka/kafka_${KAFKA_VER_1}-${KAFKA_VER_2}/bin/kafka-server-start.sh /opt/kafka/kafka_${KAFKA_VER_1}-${KAFKA_VER_2}/config/server.properties
cd /opt/kafka/kafka_${KAFKA_VER_1}-${KAFKA_VER_2}/logs/
ls -al
sudo netstat -ao | grep -i "9092"
ps -ef | grep kafka



echo "----------------------- # 9864 Hadoop1_4 !!! # ----------------------------"




sudo netstat -ao | grep -i "9864"
ps -ef | grep Hadoop

echo "----------------------- # 9870 Hadoop1_2 !!! # ----------------------------"




sudo netstat -ao | grep -i "9870"
ps -ef | grep hadoop




echo "################### CHECK WHAT IS STARTED NOW ###################"
pwd
cd $PROJECT_HOME
pwd
source ./_ubuntu_DO_ALL/96_01_check_started_services_now.sh


