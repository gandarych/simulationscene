#!/bin/bash

## 02_07_install_secrets.sh on ubuntu

echo "pwd =" $(pwd)
echo "USER_NAME_IN_OS = $USER_NAME_IN_OS, hostname =" $(hostname)
echo "date =" $(date)
echo "HOME = $HOME, USER = $USER, LOGNAME = $LOGNAME"
echo "By value: KAFKA_PORT = " $KAFKA_PORT ${KAFKA_PORT} "$KAFKA_PORT ${KAFKA_PORT}"
echo "By value: TOMCAT_VER = " $TOMCAT_VER ${TOMCAT_VER} "$TOMCAT_VER ${TOMCAT_VER}"

if [[ ! -z "$(cut -d= -f1 /home/$USER_NAME_IN_OS/.bashrc | grep declare)" ]]
then
	echo 'Environment variables are set.'
		echo "$(cut -d= -f1 /home/$USER_NAME_IN_OS/.bashrc | grep declare)"
fi

echo "%%%%%%%%%%%%%%%%%%"

pwd
cd $PROJECT_HOME
pwd
echo "RUNNING 02_07_install_secrets.sh"

## add-apt-repository -y = silent execution of this command
## >> /tmp/output.txt
## The will tell Bash to redirect stderr to stdout.
# sudo add-apt-repository -y ppa:fossproject/ppa >> /tmp/output.txt
## Or use output redirection to /dev/null and include stderr output.
# sudo add-apt-repository -y ppa:fossproject/ppa >> /dev/null
# https://phoenixnap.com/kb/how-to-install-vault-ubuntu

# https://www.vaultproject.io/
# copy link location

sudo apt install -y unzip
cd /tmp
wget https://releases.hashicorp.com/vault/${VAULT_VER}/vault_${VAULT_VER}_linux_amd64.zip
unzip vault_${VAULT_VER}_linux_amd64.zip
sudo mv vault /usr/bin
vault -v
vault --help

## vault??  > /home/${USER_NAME_IN_OS}/KD/_KDPROJ/tmp/LOGS/vault_active.log
## echo "tail -f /home/${USER_NAME_IN_OS}/KD/_KDPROJ/tmp/LOGS/vault_active.log" >> ./08_02_sudo_kubeadm_join.sh


echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo "********************************************************************************"
