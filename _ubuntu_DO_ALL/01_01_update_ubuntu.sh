#!/bin/bash

## 01_01_update_ubuntu.sh

echo "pwd =" $(pwd)
echo "USER_NAME_IN_OS = $USER_NAME_IN_OS, hostname =" $(hostname)
echo "date =" $(date)
echo "HOME = $HOME, USER = $USER, LOGNAME = $LOGNAME"
echo "By value: KAFKA_PORT = " $KAFKA_PORT ${KAFKA_PORT} "$KAFKA_PORT ${KAFKA_PORT}"
echo "By value: TOMCAT_VER = " $TOMCAT_VER ${TOMCAT_VER} "$TOMCAT_VER ${TOMCAT_VER}"

if [[ ! -z "$(cut -d= -f1 /home/$USER_NAME_IN_OS/.bashrc | grep declare)" ]]
then
	echo 'Environment variables are set.'
		echo "$(cut -d= -f1 /home/$USER_NAME_IN_OS/.bashrc | grep declare)"
fi

echo "%%%%%%%%%%%%%%%%%%"

pwd
cd $PROJECT_HOME
pwd

## add-apt-repository -y = silent execution of this command
## >> /tmp/output.txt
## The will tell Bash to redirect stderr to stdout.
# sudo add-apt-repository -y ppa:fossproject/ppa >> /tmp/output.txt
## Or use output redirection to /dev/null and include stderr output.
# sudo add-apt-repository -y ppa:fossproject/ppa >> /dev/null
#To redirect stdout to one file and stderr to another file: command > out 2>error
# sudo add-apt-repository -y ppa:fossproject/ppa >> /tmp/output.txt 2> /tmp/error.txt
#To redirect both stderr and stdout (standard output): command &> output.txt
# sudo add-apt-repository -y ppa:fossproject/ppa &> /tmp/both_output.txt
## cat /etc/fstab

sudo groupadd ${MISE_GROUP_NAME}
## ADDING A NEW GROUP TO USER:
sudo usermod -a -G "$MISE_GROUP_NAME" ${USER_NAME_IN_OS}
sudo usermod -a -G "$MISE_GROUP_NAME" root
echo "id $USER_NAME_IN_OS"
id $USER_NAME_IN_OS
echo "groups $USER_NAME_IN_OS"
groups $USER_NAME_IN_OS
echo "id root"
id root
echo "groups root"
groups root

echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
############### 01_01 UBUNTU ##############################
## 01_00_install_UBUNTU 18.04.4 LTS
## Installing "Ubuntu 18.04.4 LTS (Bionic Beaver)"

############### 01_02 UPDATE UBUNTU #######################
## Update Ubuntu (+ adding google-chrome-stable_current_amd64.deb, etc.)
## 01_01_update_ubuntu to the latest stable version
echo "RUNNING 01_02_update_ubuntu.sh"

## installer:
#### https://gist.github.com/waleedahmad/a5b17e73c7daebdd048f823c68d1f57a


## http://manpages.ubuntu.com/manpages/xenial/man8/adduser.8.html
## https://www.computerhope.com/unix/adduser.htm
## https://expertlinux.eu/2016/03/29/co-to-jest-sticky-bit-suid-i-sgid/
## https://en.wikipedia.org/wiki/Sticky_bit
## https://pl.wikipedia.org/wiki/Sticky_bit
## https://linuxexpert.pl/posts/2132/prawa-dostepu-do-plikow/
## https://www.thegeekstuff.com/2011/02/sticky-bit-on-directory-file/
## https://www.thegeekstuff.com/2013/02/sticky-bit/

## see more: https://tecadmin.net/tutorial/bash-scripting/

# accept update of ubuntu if any dialog has appeared. And restart it. And then...

echo "apt-get update -y"
sudo apt-get update -y
echo "apt-get upgrade -y"
sudo apt-get upgrade -y

echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
# install "Notepad-Plus-Plus"
echo "snap install notepad-plus-plus"
snap install notepad-plus-plus
# sudo snap install notepad-plus-plus

echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
# install git
echo "apt install -y git"
sudo apt install -y git

git config --global user.email "gandarych@gmail.com"
git config --global user.name "Gandarych"

echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo "apt install -y net-tools"
sudo apt install -y net-tools
echo "ifconfig"
ifconfig

echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
sudo rm -r /home/$USER_NAME_IN_OS/KD/_KDPROJ/tmp
mkdir /home/$USER_NAME_IN_OS/KD/_KDPROJ/tmp
mkdir /home/$USER_NAME_IN_OS/KD/_KDPROJ/tmp/LOGS
sudo chmod 0775 /home/${USER_NAME_IN_OS}/KD/_KDPROJ/tmp/LOGS

mkdir /home/${USER_NAME_IN_OS}/KD/_KDPROJ/tmp/zoo1
mkdir /home/${USER_NAME_IN_OS}/KD/_KDPROJ/tmp/zoo2
mkdir /home/${USER_NAME_IN_OS}/KD/_KDPROJ/tmp/zoo3
mkdir /home/${USER_NAME_IN_OS}/KD/_KDPROJ/tmp/packer-docker
mkdir /tmp/packer-docker
mkdir /tmp/maven_conf
# It will be used in future scripts... during the maven's installation
cp -pr ./conf/packer-docker/*.* /tmp/packer-docker
cp -pr ./conf/packer-docker/*.* /home/${USER_NAME_IN_OS}/KD/_KDPROJ/tmp/packer-docker
cp -pr ./conf/.gitconfig /home/${USER_NAME_IN_OS}/KD/_KDPROJ/tmp
cp -pr ./conf/.gitconfig /home/${USER_NAME_IN_OS}
cp -pr ./conf/settings.xml /tmp/maven_conf
cp -pr ./conf/settings.xml /home/${USER_NAME_IN_OS}/KD/_KDPROJ/tmp
cp -pr ./conf/zoo1/zoo.cfg /home/${USER_NAME_IN_OS}/KD/_KDPROJ/tmp/zoo1
cp -pr ./conf/zoo1/java.env /home/${USER_NAME_IN_OS}/KD/_KDPROJ/tmp/zoo1
cp -pr ./conf/zoo2/zoo.cfg /home/${USER_NAME_IN_OS}/KD/_KDPROJ/tmp/zoo2
cp -pr ./conf/zoo2/java.env /home/${USER_NAME_IN_OS}/KD/_KDPROJ/tmp/zoo2
cp -pr ./conf/zoo3/zoo.cfg /home/${USER_NAME_IN_OS}/KD/_KDPROJ/tmp/zoo3
cp -pr ./conf/zoo3/java.env /home/${USER_NAME_IN_OS}/KD/_KDPROJ/tmp/zoo3

echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
# install chrome
echo "cd /tmp"
cd /tmp
echo "wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb"
sudo wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
echo "dpkg -i google-chrome-stable_current_amd64.deb"
sudo dpkg -i google-chrome-stable_current_amd64.deb
## CHECK if chrome is installed here:   CHROME_HOME=/opt/google/chrome/

echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
pwd
cd $PROJECT_HOME
pwd

## To enable silent mode of installation and avoid every "Do you want to continue? [Y/n] y"
## read https://libre-software.net/ubuntu-automatic-updates/ and ...
## ... install the unattended-upgrades package on your UBUNTU
## ... or try "sudo apt install -y ... " with "-y" mode if it is eassier and possible.

echo "apt install -y unattended-upgrades"
sudo apt install -y unattended-upgrades

# sudo vi /etc/apt/apt.conf.d/50unattended-upgrades

echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo "********************************************************************************"
