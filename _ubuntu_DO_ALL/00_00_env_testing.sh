
## https://unix.stackexchange.com/questions/79064/how-to-export-variables-from-a-file/79065
## https://www.digitalocean.com/community/tutorials/how-to-read-and-set-environmental-and-shell-variables-on-a-linux-vps
## https://gist.github.com/mihow/9c7f559807069a03e302605691f85572

sudo echo "%%%%%%%%% 21 %%%%%%%%%"
sudo echo "USER_NAME_IN_OS = $USER_NAME_IN_OS"
sudo echo "date =" $(date)
sudo echo "HOME = $HOME, USER = $USER, LOGNAME = $LOGNAME"
sudo echo "hostname =" $(hostname)
sudo echo "pwd =" $(pwd)
sudo echo "By value:" $KAFKA_PORT ${KAFKA_PORT} "$KAFKA_PORT ${KAFKA_PORT}"
sudo echo "By reference:" '$KAFKA_PORT' '${KAFKA_PORT}'
sudo echo "By value:" $TOMCAT_VER ${TOMCAT_VER} "$TOMCAT_VER ${TOMCAT_VER}"
sudo echo "By reference:" '$TOMCAT_VER' '${TOMCAT_VER}'


echo "%%%%%%%%% 22 %%%%%%%%%"
echo "USER_NAME_IN_OS = $USER_NAME_IN_OS"
echo "date =" $(date)
echo "HOME = $HOME, USER = $USER, LOGNAME = $LOGNAME"
echo "hostname =" $(hostname)
echo "pwd =" $(pwd)
echo "By value:" $KAFKA_PORT ${KAFKA_PORT} "$KAFKA_PORT ${KAFKA_PORT}"
echo "By reference:" '$KAFKA_PORT' '${KAFKA_PORT}'
echo "By value:" $TOMCAT_VER ${TOMCAT_VER} "$TOMCAT_VER ${TOMCAT_VER}"
echo "By reference:" '$TOMCAT_VER' '${TOMCAT_VER}'

echo "%%%%%%%%% 23 %%%%%%%%%"

if [[ ! -z "$(cut -d= -f1 /home/$USER_NAME_IN_OS/.bashrc | grep declare)" ]]
then
	echo 'Environment variables are set.'
		echo "$(cut -d= -f1 /home/$USER_NAME_IN_OS/.bashrc | grep declare)"
fi
