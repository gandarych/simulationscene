package pl.com.microservices.simulation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@SpringBootApplication
//@EnableAutoConfiguration
//@Configuration(proxyBeanMethods = false)
@ComponentScan(basePackages = "eu.microwebservices.simulation")
public class SimulationApp {

	public static void main(String[] args) {
		SpringApplication.run(SimulationApp.class, args);
	}

	@GetMapping("/hllo")
	public String hello(@RequestParam(value = "name", defaultValue = "World") String name) {
		return String.format("Hllo %s! Simulation app is alive!", name);
	}

	@GetMapping("/hi")
	public String hi(@RequestParam(value = "name", defaultValue = "World") String name) {
		return String.format("Hi %s! Simulation app is alive!", name);
	}

}

