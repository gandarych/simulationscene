#!/bin/bash

## 03_02_systemctl_start_tomcat on ubuntu

echo "pwd =" $(pwd)
echo "USER_NAME_IN_OS = $USER_NAME_IN_OS, hostname =" $(hostname)
echo "date =" $(date)
echo "HOME = $HOME, USER = $USER, LOGNAME = $LOGNAME"
echo "By value: KAFKA_PORT = " $KAFKA_PORT ${KAFKA_PORT} "$KAFKA_PORT ${KAFKA_PORT}"
echo "By value: TOMCAT_VER = " $TOMCAT_VER ${TOMCAT_VER} "$TOMCAT_VER ${TOMCAT_VER}"

if [[ ! -z "$(cut -d= -f1 /home/$USER_NAME_IN_OS/.bashrc | grep declare)" ]]
then
	echo 'Environment variables are set.'
		echo "$(cut -d= -f1 /home/$USER_NAME_IN_OS/.bashrc | grep declare)"
fi

echo "%%%%%%%%%%%%%%%%%%"

pwd
cd $PROJECT_HOME
pwd
echo "RUNNING 03_02_systemctl_start_tomcat.sh"

## add-apt-repository -y = silent execution of this command
## >> /tmp/output.txt
## The will tell Bash to redirect stderr to stdout.
# sudo add-apt-repository -y ppa:fossproject/ppa >> /tmp/output.txt
## Or use output redirection to /dev/null and include stderr output.
# sudo add-apt-repository -y ppa:fossproject/ppa >> /dev/null

## Reload the Systemd File
echo "sudo systemctl daemon-reload"
sudo systemctl daemon-reload

echo "&&&&&&&&&&&&&&&&&&&&01"
## Restart Tomcat
echo "sudo systemctl start tomcat"
#source ./_ubuntu_DO_ALL/00_01_EnvVAR_check_new_versions.sh
echo "Type 'q' !!!!!!!!!!!!"
####!!!!!!! sudo systemctl start tomcat > "/home/${USER_NAME_IN_OS}/KD/_KDPROJ/tmp/LOGS/tomcat_active.log"
echo "q"
##########continue:  'q'
q

echo "tail -f /home/${USER_NAME_IN_OS}/KD/_KDPROJ/tmp/LOGS/tomcat_active.log" | tee -a $PROJECT_HOME/_ubuntu_DO_ALL/08_02_sudo_kubeadm_join.sh
gnome-terminal &
source ./_ubuntu_DO_ALL/00_01_EnvVAR_check_new_versions.sh

echo "&&&&&&&&&&&&&&&&&&&&02"
## Note: If the Tomcat service fails to start use "journalctl -xn" as a way to know the exact errors that are occurring.
## Verify Tomcat is Running by the status
echo "sudo systemctl status tomcat"
sudo systemctl status tomcat
## Output:
####   * tomcat.service - Tomcat 9 servlet container
####     Loaded: loaded (/etc/systemd/system/tomcat.service; disabled; vendor preset: enabled)
####     Active: active (running) since Sun RRRR-MM-DD 14:36:58 CET; 13ms ago
####     Process: 14390 ExecStart=/opt/tomcat/apache-tomcat-9.0.33/bin/startup.sh (code=exited, status=0/SUCCESS)
####    Main PID: 14400 (java)
####      Tasks: 6 (limit: 4915)
####     CGroup: /system.slice/tomcat.service
####             └─14400 /usr/lib/jvm/java-1.8.0-openjdk-amd64//bin/java -Djava.util.logging.config.file=/opt/tomcat/apache-tomcat-9.0.33/conf/logging.properties 
####  
####  gandalf1 systemd[1]: Starting Tomcat 9 servlet container...
####  gandalf1 startup.sh[14390]: Tomcat started.
####  gandalf1 systemd[1]: Started Tomcat 9 servlet container.
####  
## If correctly installed you’ll also be able to see the Tomcat default page by visiting http://Host_IP:8080 in your browser,
## replacing Host_IP with your server’s IP or hostname, followed by Tomcat’s port number 8080.

echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo "********************************************************************************"
